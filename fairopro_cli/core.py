from fairopro import plotter
from fairopro.cfg import CfgBuilder
from fairopro.corr_time import correct_instrumenttime, set_range_t_exp
from fairopro.data_io import dump_dfs, load_data, save_data
from fairopro.process_cl import calculate_cl_o3
from fairopro.process_uv import calculate_uv_o3

plotter.BLOCK = True


def process_data(config_file, **kwargs):
    """Main processing function."""
    cfg = (
        CfgBuilder()
        .verbose(kwargs.get("verbose", True))
        .write_output(kwargs.get("write_output", False))
        .plots_close(kwargs.get("plots_close", False))
        .force_load_from_raw(kwargs.get("force_load_from_raw", False))
        .write_raw_merge_csv(kwargs.get("write_raw_merge_csv", False))
        .write_params_uv(("O3_ppb",))
        .write_params_cl(("O3_ppb",))
        .write_df_dump(kwargs.get("write_df_dump", False))
        .time_format(kwargs.get("time_format", "mdns"))
        .output_float_precision(kwargs.get("output_float_precision", 3))
        .build()
    )

    # Load configuration
    cfg.load_params(config_file)

    # Load data
    dat = load_data(cfg)

    # Process UV
    dat, cfg = calculate_uv_o3(dat, cfg)

    # Process CL
    if cfg.params.process_cl and cfg.params.derived.uv_nvd > 3:
        dat, cfg = calculate_cl_o3(dat, cfg)

    # Set experiment time range
    if cfg.params.t_range_exp:
        dat, cfg = set_range_t_exp(dat, cfg)

    # Map time to IWG1 or manual fit/offset
    if cfg.params.process_t_corr:
        dat, cfg = correct_instrumenttime(dat, cfg)

    # Plots
    plotter.call_4_plots(dat, cfg)

    # Output
    if cfg.write_output:
        save_dat = save_data(dat, cfg).resolve()
        print(f"+++++\n+ data saved in\n+ {save_dat.as_posix()}")
        save_cfg = cfg.save_params().resolve()
        print(f"+++++\n+ cfg file saved as {save_cfg.name} in\n+ {save_cfg.parent.as_posix()}")

    if cfg.write_df_dump:
        dst = dump_dfs(dat, cfg, cfg.write_df_dump)
        print(f"+++++\n+ dataframes dumped to\n+ {dst.as_posix()}")
