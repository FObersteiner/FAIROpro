from pathlib import Path

import click

from .core import process_data


@click.command()
@click.argument("config_file", type=click.Path(exists=True, path_type=Path))
@click.option("--quiet", is_flag=True, help="Disable verbose output")
@click.option("--write-output", is_flag=True, help="Write output files")
@click.option("--close-plots", is_flag=True, help="Close plots automatically")
@click.option("--force-load-raw", is_flag=True, help="Force loading from raw data")
@click.option("--write-raw-merge", is_flag=True, help="Write raw merge CSV")
@click.option("--write-df-dump", is_flag=True, help="Dump dataframes")
@click.option("--time-format", default="mdns", help="Time format to use")
@click.option("--float-precision", default=3, help="Output float precision")
def main(
    config_file,
    quiet,
    write_output,
    close_plots,
    force_load_raw,
    write_raw_merge,
    write_df_dump,
    time_format,
    float_precision,
):
    """Process FAIRO data using the specified configuration file."""
    process_data(
        config_file,
        verbose=~quiet,
        write_output=write_output,
        plots_close=close_plots,
        force_load_from_raw=force_load_raw,
        write_raw_merge_csv=write_raw_merge,
        write_df_dump=write_df_dump,
        time_format=time_format,
        output_float_precision=float_precision,
    )


if __name__ == "__main__":
    main()
