# Version Mgmt

## documents that specify the project (current) version

- ./CHANGELOG.md
- ./pyproject.toml
- ./CITATION.cff

## procedure for release

- update version in all aforementioned documents
- update/add DOI in CITATION.cff
- verify that tests and linting run without errors
- push updates
- set the version as `git tag` and push the tag
- create a release on gitlab
- revise publication on zenodo
