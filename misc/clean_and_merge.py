"""
searches for xx_xx_xx directories in current directory.

runs V25 cleaner for all specified file types.

creates a merge csv for each file type.
"""

import re
from pathlib import Path

# from matplotlib import pyplot as plt
# import pandas as pd
# from pyfuppes.interpolate import pd_interp_df
from fairopro.v25 import (
    PATH_V25_DATA_CFG,
    collect_OSC_Logs,
    collect_V25Logs,
    logs_cleanup,
)

VERBOSE = True

# directory where to look for subdirectories with FAIRO data
srcdirs = list(Path(".").resolve().glob("*"))


# find all subdirectories with V25 logs
srcdirs = [d for d in srcdirs if re.match(r"^[0-9]{2}_[0-9]{2}_[0-9]{2}$", d.name)]

# clean-up
logs_cleanup(srcdirs, PATH_V25_DATA_CFG, verbose=VERBOSE)

# load data and make mergefiles
exts = ["t_p", "dat", "OMC", "hal", "OSC", "mas"]
data = {}
for ext in exts:
    f = collect_OSC_Logs if ext == "OSC" else collect_V25Logs
    data[ext] = f(
        srcdirs,
        ext,
        write_mergefile=True,
        verbose=VERBOSE,
    )

# # make a dataframe with a common time axis
# tnames = {
#     "t_p": "T_pTime",
#     "dat": "DateTime",
#     "OMC": "Time",
#     "hal": "V25_Time",
#     "OSC": "POSIX",
# }

# # T_p is the reference.
# k = "t_p"
# df = pd.DataFrame(data[k])
# for c in df.columns:
#     try:
#         df[c] = pd.to_numeric(df[c])
#     except ValueError:
#         pass

# df["datetime"] = pd.to_datetime(df[tnames[k]], dayfirst=True)
# # we want a seconds-after-midnight index.
# df.index = ((df["datetime"] - df["datetime"].iloc[0].floor("d")).values).astype(
#     float
# ) / 1e9

# for k in "dat", "OMC", "hal", "OSC":
#     df_tmp = pd.DataFrame(data[k])
#     for c in df_tmp.columns:
#         try:
#             df_tmp[c] = pd.to_numeric(df_tmp[c])
#         except ValueError:  # skips string type columns
#             pass

#     # make sure we have a time axis with seconds after midnight
#     if k == "OSC":
#         df_tmp[f"mdns_{k}"] = pd.to_datetime(df_tmp[tnames[k]], unit="s")
#     else:
#         df_tmp[f"mdns_{k}"] = pd.to_datetime(df_tmp[tnames[k]], dayfirst=True)

#     df_tmp[f"mdns_{k}"] = (
#         (df_tmp[f"mdns_{k}"] - df_tmp[f"mdns_{k}"].iloc[0].floor("d")).values
#     ).astype(float) / 1e9

#     df_tmp.index = df_tmp[f"mdns_{k}"]

#     # interpolate to reference df time
#     df_tmp = pd_interp_df(df_tmp, df.index)
#     # concat
#     df = pd.concat([df, df_tmp], axis=1)

#     print(f"adding {k}")
