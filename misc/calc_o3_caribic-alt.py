"""
a script to recalculate ozone mixing ratio for the old CARIBIC instrument,
that still uses a Hg lamp.

place the script in the directory that contains the data, i.e. the
yy_mm_dd subdirectory, and run.

c[ppb] = (ln(I_0/I) / (sigma_T * l * n)) * (p_0/p) * ((T+T_0)/T_0) * 1e9
"""

import re

# import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pyfuppes.interpolate import pd_Series_ip1d

# constants
from fairopro.process_uv import P0, SIGMA, T0, TCORRFITPARMS, L, N
from fairopro.v25 import logs_cleanup

try:
    wd = Path(__file__).parent
except NameError:
    wd = Path.cwd()  # same as os.getcwd()
assert wd.is_dir(), "faild to obtain working directory"


wd = (wd / "../v25data/2023-03-01").resolve()

############################################################################
# 1 - preprocessing
############################################################################

VERBOSE = True
WRITE_MERGE = True

# directory where to look for subdirectories with FAIRO data
srcdirs = list(wd.glob("*"))

# find all subdirectories with V25 logs
srcdirs = [d for d in srcdirs if re.match(r"^[0-9]{2}_[0-9]{2}_[0-9]{2}$", d.name)]

# clean-up
logs_cleanup(
    srcdirs,
    drop_info=True,
    check_info=True,
    add_osc_datetime=True,
    verbose=VERBOSE,
)

# to df
df_dat = pd.concat((pd.read_csv(f, sep="\t") for f in sorted(srcdirs[0].glob("*.DAT"))))
df_tp = pd.concat((pd.read_csv(f, sep="\t") for f in sorted(srcdirs[0].glob("*.T_P"))))


############################################################################
# 2 - merge .dat and .t_p
############################################################################


df_dat["DateTime"] = pd.to_datetime(df_dat["DateTime"], format="%d.%m.%y %H:%M:%S.%f")
ref_date = df_dat["DateTime"].iloc[0].floor("D")
df_dat["mdns"] = (df_dat["DateTime"] - ref_date).values.astype(int) / 1e9

df_tp["T_pTime"] = pd.to_datetime(df_tp["T_pTime"], format="%d.%m.%y %H:%M:%S.%f")
df_tp["mdns"] = (df_tp["T_pTime"] - ref_date).values.astype(int) / 1e9

# interpolate all Series from t_p to dat time scale
df = df_dat.copy()
keys = df_tp.columns.drop(["T_pTime", "mdns"])
for k in keys:
    df = pd_Series_ip1d(df_tp, df, "mdns", k, "mdns", k)

if WRITE_MERGE:
    # make csv output similar to that of pyfairoproc
    df.to_csv(srcdirs[0] / (srcdirs[0].name + "_merged.txt"), index=False, sep="\t")


############################################################################
# 3 - re-calculate ozone mixing ratio
############################################################################


df = df.rename(columns={"O3": "O3_V25"})

# TODO: the following does not *always* work correctly:

# # reindex df to ensure alternating o3 / zero air switch between cuvettes
# t_interval = 6 # t flush + t measure
# df = df.set_index(df["mdns"].astype(int))
# df = df[~df.index.duplicated()]
# idx0 = df.index[0] - df.index[0] % t_interval
# df = df.reindex(
#     pd.Index([idx0 + i * 5 for i in range(int((df.index[-1] - df.index[0]) / 5))])
# )

# make 3p look-ahead averages
df["p_cuv_avg"] = df["p_Cuev"].rolling(window=3).mean().shift(-2)
df["T_cuv_avg"] = (
    df["T_Cuev1"].rolling(window=3).mean().shift(-2)
    + df["T_Cuev2"].rolling(window=3).mean().shift(-2)
) / 2

df["sigmaT"] = SIGMA * (
    1
    + TCORRFITPARMS[4] * df["T_cuv_avg"] ** 4
    + TCORRFITPARMS[3] * df["T_cuv_avg"] ** 3
    + TCORRFITPARMS[2] * df["T_cuv_avg"] ** 2
    + TCORRFITPARMS[1] * df["T_cuv_avg"]
    + TCORRFITPARMS[0]
)

df.loc[(df["State"] != "MS") | (df["Signal1"] < 1e6), "Signal1"] = np.NaN
df.loc[(df["State"] != "MS") | (df["Signal2"] < 1e6), "Signal2"] = np.NaN

# shift signals by 1 and 2 elements
df["Signal1+1"] = df["Signal1"].shift(-1)
df["Signal1+2"] = df["Signal1"].shift(-2)
df["Signal2+1"] = df["Signal2"].shift(-1)
df["Signal2+2"] = df["Signal2"].shift(-2)


I0avg = df["Signal1"] / df["Signal2"] + df["Signal1+2"] / df["Signal2+2"]
Iavg = (df["Signal1+1"] / df["Signal2+1"]) * 2

df["O3_recalc"] = (
    1e9
    * P0
    / df["p_cuv_avg"]
    * (df["T_cuv_avg"] + T0)
    / T0
    * (1 / (df["sigmaT"] * L * 2 * N))
    * np.log(I0avg / Iavg)
).shift(2)


# TODO : could change sequence of cuvettes for certain sections of the experiment
df["inverter"] = pd.Series([1, -1] * int(df.size // 2))
df["O3_recalc"] *= df["inverter"]


df.loc[df["O3_V25"] > 20000, "O3_V25"] = np.nan
df.loc[df["O3_V25"] < -200, "O3_V25"] = np.nan
# df.loc[df["O3_recalc"] < -20, "O3_recalc"] = np.nan
# df.loc[df["O3_recalc"] > 20000, "O3_recalc"] = np.nan

df["diffO3"] = df["O3_recalc"] - df["O3_V25"]
df.loc[(df["diffO3"] < -10), "O3_recalc"] *= -1


plt.plot(df["DateTime"], df["O3_V25"], "b", label="V25 O3")
plt.plot(df["DateTime"], df["O3_recalc"], "g", label="recalc. O3")
plt.plot(df["DateTime"], df["O3_recalc"] - df["O3_V25"], "r", label="diff")
plt.grid()
plt.legend()
plt.show()


############################################################################
# 4 - dump to csv
############################################################################

if WRITE_MERGE:
    # select and re-arrange columns
    header = [
        "DateTime",
        "mdns",
        "State",
        "O3_V25",
        "O3_recalc",
        "Signal1",
        "Signal2",
        "sigmaT",
        "Temperature",
        "T_Optic",
        "T_Cuev1",
        "T_Cuev2",
        "T_cuv_avg",
        "T_O3Scr",
        "T_PumpPre",
        "T_PumpOsc",
        "T_Van",
        "T_uvLamp",
        "p_Inlet",
        "p_Cuev",
        "p_cuv_avg",
        "p_preOSC",
        "dp_Oscar",
        "f_OMCAL",
        "OSC_Caps",
        "PowH_Optic",
        "PowH_O3Scr",
        "PowPumpPre",
        "PowPumpOsc",
        "Q_Van",
        "t_Interval",
        "t_PC_ans",
    ]
    df.to_csv(
        srcdirs[0] / (srcdirs[0].name + "_recalc.txt"),
        index=False,
        sep="\t",
        columns=header,
    )
