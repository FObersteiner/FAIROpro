#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

# params
t_flush = 2.0  # s
t_meas = 2.0
t_shift = 0.2

# events, initial values
switch_valve = 0
trigger_pc = 0

event_valve = []
event_trigger = []
event_receive = []


for i in range(50):
    trigger_pc = switch_valve + t_flush + t_shift
    # wait trigger pc
    switch_valve += t_flush + t_meas
    # wait switch valves
    event_trigger.append(trigger_pc)
    event_receive.append(trigger_pc + t_meas)
    event_valve.append(switch_valve)


plt.scatter(event_valve, np.ones(len(event_valve)), label="switch valve")
plt.scatter(event_trigger, np.ones(len(event_trigger)) - 0.002, label="send trigger")
plt.scatter(event_receive, np.ones(len(event_receive)) + 0.002, label="receive data")
plt.ylim([0.99, 1.01])
plt.legend()
plt.grid()
plt.show()
