# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from scipy import stats

ndigits = 6


# ~~~ pressure ~~~

y = [1003.6, 0.2]  # target

# FARIO
# p inlet
x = [297.37, -0.27]
slope, intercept, *_ = stats.linregress(x, y)
print(f"pInlet_Gain   {slope:.{ndigits}f}")
print(f"pInlet_Offs   {intercept:.{ndigits}f}")

# p cuev
x = [561.52, 116.33]
slope, intercept, *_ = stats.linregress(x, y)
print(f"pCuev_Gain   {slope:.{ndigits}f}")
print(f"pCuev_Offs   {intercept:.{ndigits}f}")

# p pre osc
x = [297.40, 122.6]
slope, intercept, *_ = stats.linregress(x, y)
print(f"ppreOsc_Gain   {slope:.{ndigits}f}")
print(f"ppreOsc_Offs   {intercept:.{ndigits}f}")


# ~~~ flow ~~~


# fOMCAL
# linear fit (Fairo-3 / Caribic)
# (!) make cali in SB mode
y = [0.05, 1.94, 2.93, 3.96, 4.99]  # ref flow meter
x = [2.602, 3.508, 4.040, 4.602, 5.170]  # indicated by V25

y = [i - y[0] for i in y]  # offset correction
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)

plt.scatter(x, y, color="r")
plt.plot(x, [i * slope + intercept for i in x], color="r")
plt.show()

print(f"fOMCAL_Gain   {slope:.{ndigits}f}")
print(f"fOMCAL_Offs   {intercept:.{ndigits}f}")
print(
    "    r_value, p_value, std_err: "
    + f'{" ".join(f"{x:.6f}" for x in (r_value, p_value, std_err))}'
)

# CARIBIC alt: dpOMCAL
y = [0.05, 1.25, 2.13, 2.83, 3.99]
x = [-0.11, 2.18, 4.60, 7.05, 12.04]

y = [i - y[0] for i in y]  # offset correction
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)

plt.scatter(x, y, color="b")
plt.plot(x, [i * slope + intercept for i in x], color="b")
plt.show()

print(f"ppreOsc_Gain   {slope:.{ndigits}f}")
print(f"ppreOsc_Offs   {intercept:.{ndigits}f}")
print(
    "    r_value, p_value, std_err: "
    + f'{" ".join(f"{x:.6f}" for x in (r_value, p_value, std_err))}'
)
