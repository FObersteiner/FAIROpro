"""
send CARIBIC master message to specified serial interface.

provides a convenient way to sync the V25 clock with that of the PC.
"""

import struct
import time
from datetime import datetime, timezone

import serial


def xor_checksum(b: bytes) -> bytes:
    cs = 0
    for i in b:
        cs ^= i
    return struct.pack("B", cs)


list_of_bytes = [2, 50, 49, 54, 44, 49, 52, 58, 48, 48, 58, 52, 50, 44, 73, 78, 3, 50]
b = [struct.pack("B", i) for i in list_of_bytes[1:-1]]
assert struct.unpack("B", xor_checksum(b"".join(b)))[0], list_of_bytes[-1]

# teststrings = [  # b'\x02hello\x03a', b'\x02hello\x03a', b'\x02hello\x03a',
#     "\x02216,10:09:57.06,SB\x03\x05",
#     "\x02216,10:09:57.06,SB\x03\x05",
#     "\x02216,10:09:57.06,SB\x03\x05",
# ]

# # configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(
    # port="COM12",
    # baudrate=9600,
    port="/dev/ttyUSB0",
    baudrate=9600,
)

for _ in range(10):
    ts = datetime.now(timezone.utc).strftime("%H:%M:%S.%f")[:-4]

    msg = f"217,{ts},SB\x03"

    msg = msg.encode()

    msg += xor_checksum(msg)

    msg = b"\x02" + msg

    print(f"{ts}: sending {msg}")
    # out = b""

    ser.write(msg)
    # time.sleep(0.5)

    # out += ser.read(128)
    # if out != "":
    #    print(f"{datetime.now(timezone.utc).strftime('%H:%M:%S.%f')[:-4]} >>" + out)

    time.sleep(3)

ser.close()
