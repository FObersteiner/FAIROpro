# -*- coding: utf-8 -*-
"""
FAIRO data processing, main script. This is a DEMO.

Create a main script for each experiment (could also be a series of measurements).
"""
from pathlib import Path

from fairopro.cfg import CfgBuilder
from fairopro.corr_time import correct_instrumenttime, set_range_t_exp
from fairopro.data_io import dump_dfs, load_data, save_data

# import numpy as np
# import pandas as pd
# import polars as pl
# import scipy as sc
from fairopro.plotter import call_4_plots
from fairopro.process_cl import calculate_cl_o3
from fairopro.process_uv import calculate_uv_o3

init_dir = "tests/testdata/"
cfg_files = [
    "cfg_with_valid_path.toml",
]
cfg_files = [Path(init_dir) / f for f in cfg_files]


file = Path(cfg_files[0])

# --- prepare the cfg
cfg = (
    CfgBuilder()
    .verbose(True)
    .write_output(False)
    .plots_close(False)
    .force_load_from_raw(False)
    .write_raw_merge_csv(False)
    .write_params_uv(("O3_ppb",))
    .write_params_cl(("O3_ppb",))
    .write_df_dump(False)
    .time_format("mdns")
    .output_float_precision(3)
    .build()
)

# --- LOAD CFG file
cfg.load_params(file)

# --- LOAD data
dat = load_data(cfg)

# --- PROCESS UV
dat, cfg = calculate_uv_o3(dat, cfg)

# --- PROCESS CL
if cfg.params.process_cl and cfg.params.derived.uv_nvd > 3:
    dat, cfg = calculate_cl_o3(dat, cfg)

# --- SET EXPERIMENT TIME RANGE
if cfg.params.t_range_exp:
    dat, cfg = set_range_t_exp(dat, cfg)

# --- MAP TIME TO IWG1 or manual fit/offset
if cfg.params.process_t_corr:
    dat, cfg = correct_instrumenttime(dat, cfg)

# --- PLOTS
call_4_plots(dat, cfg)

# --- OUTPUT
if cfg.write_output:
    save_dat = save_data(dat, cfg).resolve()
    print(f"+++++\n+ data in\n+ {save_dat.as_posix()}")
if cfg.write_output:
    save_cfg = cfg.save_params().resolve()
    print(f"+++++\n+ cfg file saved as {save_cfg.name} in\n+ {save_cfg.parent.as_posix()}")
if cfg.write_df_dump:
    dst = dump_dfs(dat, cfg, cfg.write_df_dump)
    print(f"+++++\n+ dataframes dumped to\n+ {dst.as_posix()}")

# -----------------------------------------------
dftp = dat["tp"].to_pandas()
dfuv = dat["uv"].to_pandas()
dfcl = dat["cl"].to_pandas()
dfac = dat["ac"].to_pandas()
