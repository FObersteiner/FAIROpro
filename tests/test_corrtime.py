# -*- coding: utf-8 -*-
import pathlib
import unittest

import polars as pl

from fairopro.cfg import CfgBuilder
from fairopro.corr_time import Flags_T, correct_instrumenttime, parse_reftime
from fairopro.data_io import load_data


class TestCfg(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            cls.wd = pathlib.Path(__file__).parent
        except NameError:
            cls.wd = pathlib.Path.cwd()
        assert cls.wd.is_dir(), "faild to obtain working directory"

    @classmethod
    def tearDownClass(cls):
        # to run after all tests
        pass

    def setUp(self):
        # to run before each test
        pass

    def tearDown(self):
        # to run after each test
        pass

    def test_flags(self):
        # ok means no bit set:
        self.assertEqual(Flags_T.OK, 0)
        # all flags must be a power of 2 or 0
        self.assertTrue(all((int(x) & (int(x) - 1)) == 0 for x in Flags_T))

    def test_parsereft(self):
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        cfg.params.timing.use_t_ref = False
        dat = load_data(cfg)
        self.assertTrue(dat["ac"] is not None)
        # initially, reference time is not parsed
        self.assertEqual(dat["ac"]["HALO_Time"].dtype, pl.Utf8)
        # parse to datetime
        dat["ac"] = parse_reftime(cfg, dat["ac"])
        self.assertTrue("ref_time" in dat["ac"].columns)

    def test_corrtime(self):
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        dat = load_data(cfg)

        # if process_t_corr is false, calling it makes no sense
        cfg.params["process_t_corr"] = False
        with self.assertRaises(ValueError):
            correct_instrumenttime(dat, cfg)

        # neither ref time nor user specified should also fail
        cfg.params["process_t_corr"] = True
        cfg.params.timing["t_corr_params_userdef"] = False
        cfg.params.timing["t_offset_userdef"] = False
        cfg.params.timing["use_t_ref"] = False
        with self.assertRaises(ValueError):
            correct_instrumenttime(dat, cfg)

        # reference time should work in the test data experiment
        cfg.params.timing["use_t_ref"] = True
        self.assertTrue(dat["ac"] is not None)
        self.assertEqual(dat["ac"]["HALO_Time"].dtype, pl.Utf8)
        dat, cfg = correct_instrumenttime(dat, cfg)
        self.assertTrue("dt" in dat["ac"].columns)
        self.assertTrue("t_corr" in dat["ac"].columns)

        # test diff flag - invalid range leaves no valid data
        cfg.params.timing["t_ref_delta_range"] = [-3600, -360]
        with self.assertRaises(ValueError):
            correct_instrumenttime(dat, cfg)

        # user defined parameters must propagate to derived
        cfg.params.timing["t_ref_delta_range"] = [-3600, 3600]
        cfg.params.timing["use_t_ref"] = False
        cfg.params.timing["t_corr_params_userdef"] = [1, 0]
        cfg.params.timing["t_offset_userdef"] = 1
        dat, cfg = correct_instrumenttime(dat, cfg)
        self.assertListEqual([1, 0], cfg.params.derived.t_corr_fitparams)
        self.assertEqual(1, cfg.params.derived.t_corr_offset)

        # test offset propagates to data
        cfg.params.timing["t_ref_delta_range"] = [-3600, 3600]
        cfg.params.timing["use_t_ref"] = False
        cfg.params.timing["t_corr_params_userdef"] = False
        cfg.params.timing["t_offset_userdef"] = 1
        dat = load_data(cfg)
        dat, cfg = correct_instrumenttime(dat, cfg)
        self.assertAlmostEqual((dat["ac"]["mdns_uncorr"] - dat["ac"]["mdns"]).mean(), 1)


if __name__ == "__main__":
    unittest.main()
