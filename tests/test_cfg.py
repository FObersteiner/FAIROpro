# -*- coding: utf-8 -*-
import pathlib
import unittest

from fairopro.cfg import CfgBuilder, dotdict


class TestCfg(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            cls.wd = pathlib.Path(__file__).parent
        except NameError:
            cls.wd = pathlib.Path.cwd()  # same as os.getcwd()
        assert cls.wd.is_dir(), "faild to obtain working directory"

    @classmethod
    def tearDownClass(cls):
        # to run after all tests
        pass

    def setUp(self):
        # to run before each test
        pass

    def tearDown(self):
        # to run after each test
        pass

    def test_CfgBuilder(self):
        cfg = (
            CfgBuilder()
            .verbose(1)
            .write_output(0)
            .plots_close(0)
            .force_load_from_raw(0)
            .write_raw_merge_csv(0)
            .write_params_uv(("O3_ppb",))
            .write_params_cl(("O3_ppb",))
            .write_df_dump(0)  # {0, "csv", "parquet"}
            .time_format("mdns")
            .output_float_precision(3)
            .build()
        )

        self.assertEqual(cfg.verbose, 1)
        self.assertEqual(cfg.write_output, 0)
        self.assertEqual(cfg.plots_close, 0)
        self.assertEqual(cfg.force_load_from_raw, 0)
        self.assertEqual(cfg.write_raw_merge_csv, 0)
        self.assertEqual(cfg.write_params_uv, ("O3_ppb",))
        self.assertEqual(cfg.write_params_cl, ("O3_ppb",))
        self.assertEqual(cfg.write_df_dump, 0)
        self.assertEqual(cfg.time_format, "mdns")
        self.assertEqual(cfg.output_float_precision, 3)
        self.assertEqual(cfg.params, dotdict({}))

        # cannot set attribute without using the builder:
        with self.assertRaises(Exception):
            cfg.verbose = 0  # type: ignore

        # internally, we can use "_attribute":
        with self.assertRaises(Exception):
            cfg.time_format = "test"  # type: ignore
        self.assertEqual(cfg.time_format, "mdns")
        cfg._time_format = "iso"
        self.assertEqual(cfg.time_format, "iso")

        # test params dict dot notation
        self.assertTrue(isinstance(cfg.params, dotdict))
        cfg._params["test"] = 5
        self.assertEqual(cfg.params.test, 5)

    def test_Cfg_params(self):
        cfg = CfgBuilder().build()
        # test params setter / load from file
        with self.assertRaises(FileNotFoundError):
            cfg.load_params(self.wd / "test", verify_inputpaths=False)  # not a file
        with self.assertRaises(ValueError):
            cfg.load_params(self.wd / "testdata/cfg_invalid0.toml", verify_inputpaths=False)

        cfg.load_params(self.wd / "../fairopro/resources/master_cfg.toml", verify_inputpaths=False)
        self.assertEqual(
            cfg.params.derived.params_file,  # type: ignore
            (self.wd / "../fairopro/resources/master_cfg.toml").resolve().as_posix(),
        )
        self.assertEqual(cfg.params.state_coerce, "MS")
        self.assertTrue("cl" in cfg.params)
        self.assertTrue(isinstance(cfg.params.cl, dotdict))
        self.assertTrue("timing" in cfg.params)
        self.assertTrue(isinstance(cfg.params.timing, dotdict))
        self.assertTrue("uv" in cfg.params)
        self.assertTrue(isinstance(cfg.params.uv, dotdict))
        self.assertTrue("derived" in cfg.params)
        self.assertTrue(isinstance(cfg.params.derived, dotdict))

        # test path validation
        cfg = CfgBuilder().build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml", verify_inputpaths=True)
        self.assertEqual(cfg.params.path_input, self.wd / "testdata/V25/valid_input")

    def test_Cfg_legacy(self):
        cfg = CfgBuilder().build()
        cfg.load_params(
            self.wd / "testdata/cfg_legacy.yml",
            verify_inputpaths=False,
            is_legacy=True,
        )
        self.assertTrue((self.wd / "testdata/cfg_legacy_converted.toml").is_file())
        (self.wd / "testdata/cfg_legacy_converted.toml").unlink(missing_ok=False)
        self.assertEqual(cfg.params.cl.sensi_avg_win, 110)  # type: ignore
        self.assertEqual(cfg.params.state_coerce, "MS")
        self.assertTrue("cl" in cfg.params)
        self.assertTrue(isinstance(cfg.params.cl, dotdict))
        self.assertTrue("timing" in cfg.params)
        self.assertTrue(isinstance(cfg.params.timing, dotdict))
        self.assertTrue("uv" in cfg.params)
        self.assertTrue(isinstance(cfg.params.uv, dotdict))
        self.assertTrue("derived" in cfg.params)
        self.assertTrue(isinstance(cfg.params.derived, dotdict))

    def test_Cfg_save(self):
        cfg = CfgBuilder().build()
        cfg.load_params(self.wd / "../fairopro/resources/master_cfg.toml", verify_inputpaths=False)
        self.assertEqual(
            cfg.params.derived.params_file,  # type: ignore
            (self.wd / "../fairopro/resources/master_cfg.toml").resolve().as_posix(),
        )
        # test save to file
        cfg.params.path_output = self.wd / "testdata/"
        dst = cfg.save_params()
        self.assertTrue(dst.is_file())
        # cleanup
        dst.unlink(missing_ok=False)
        dst.parent.rmdir()


if __name__ == "__main__":
    unittest.main()
