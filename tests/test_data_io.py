# -*- coding: utf-8 -*-
import pathlib
import unittest
from datetime import timedelta

import polars as pl
import pytest
from polars.testing import assert_frame_equal

from fairopro.cfg import CfgBuilder
from fairopro.data_io import DAT_KEYS, USE_PYARROW, dump_dfs, load_data
from fairopro.process_cl import calculate_cl_o3
from fairopro.process_uv import calculate_uv_o3


class TestCfg(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            cls.wd = pathlib.Path(__file__).parent
        except NameError:
            cls.wd = pathlib.Path.cwd()
        assert cls.wd.is_dir(), "faild to obtain working directory"

    @classmethod
    def tearDownClass(cls):
        # to run after all tests
        pass

    def setUp(self):
        # to run before each test
        pass

    def tearDown(self):
        # to run after each test
        pass

    def test_no_data(self):
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_empty_input.toml")
        with self.assertRaises(FileNotFoundError):
            _ = load_data(cfg)

    def test_no_zipfile(self):
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_no_zipfile.toml")
        with self.assertRaises(FileNotFoundError):
            _ = load_data(cfg)

    def test_no_data_in_zip(self):
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_invalid_zipfile.toml")
        with self.assertRaises(FileNotFoundError):
            _ = load_data(cfg)

    def test_compare_zip_parquet(self):
        cfg_a = CfgBuilder().verbose(False).force_load_from_raw(1).build()
        cfg_a.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        dat_a = load_data(cfg_a)
        cfg_b = CfgBuilder().verbose(False).force_load_from_raw(0).build()
        cfg_b.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        dat_b = load_data(cfg_b)
        for k in DAT_KEYS:
            self.assertIn(k, dat_a.keys())
            self.assertIn(k, dat_b.keys())
            frame_a, frame_b = dat_a[k], dat_b[k]
            self.assertIsNone(assert_frame_equal(frame_a, frame_b))

    # for name, df in dat.items():
    #     if df is None:
    #         print(f"\n --- no data for {name} ---")
    #         continue
    #     print(f"\n--- {name} ---")
    #     for c, t in zip(df.columns, df.dtypes):
    #         print(c, t)

    def test_cl_timeregrid(self):
        # after cl time correction is applied, standard deviation of frequency
        # variability must be lower
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        cfg.params.cl.regrid_samples = False  # type: ignore
        dat = load_data(cfg)
        diff0 = dat["cl"]["v25_time"].cast(float).diff().drop_nulls() / 1e6
        diff0 = diff0.filter(diff0 < 1)
        # print(diff0.std(), diff0.max(), diff0.min())
        cfg.params.cl.regrid_samples = True  # type: ignore
        dat = load_data(cfg)
        diff1 = dat["cl"]["v25_time"].cast(float).diff().drop_nulls() / 1e6
        diff1 = diff1.filter(diff1 < 1)
        # print(diff1.std(), diff1.max(), diff1.min())
        self.assertTrue(diff1.std() < diff0.std())

    def test_datetime_monotonic(self):
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        dat = load_data(cfg)
        # all datetime columns: no jumps backwards in time
        for key in dat:
            m = dat[key]["v25_time"].diff().fill_null(timedelta(microseconds=1)) > timedelta(0)
            self.assertTrue(m.all())
        # aircraft data handled correctly: --> see utils test

    @pytest.mark.filterwarnings("ignore:divide by zero encountered in log")
    def test_data_saved(self):
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        dat = load_data(cfg)
        dat, cfg = calculate_uv_o3(dat, cfg)
        dat, cfg = calculate_cl_o3(dat, cfg)
        dst = dump_dfs(dat, cfg, dump_format="parquet")
        files = list(dst.glob("*.parquet"))
        self.assertEqual(len(files), 5)

        dat_from_pq = {
            f.stem.split("_")[-1]: pl.read_parquet(f, use_pyarrow=USE_PYARROW) for f in files
        }
        for key, df in dat_from_pq.items():
            self.assertTrue(assert_frame_equal(df, dat[key]) is None)
        for f in files:
            f.unlink(missing_ok=False)
        dst.rmdir()


if __name__ == "__main__":
    unittest.main()
