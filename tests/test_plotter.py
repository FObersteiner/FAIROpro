# -*- coding: utf-8 -*-
import pathlib
import shutil
import unittest

import fairopro.plotter as fplt
from fairopro.cfg import CfgBuilder
from fairopro.data_io import load_data


class TestCfg(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            cls.wd = pathlib.Path(__file__).parent
        except NameError:
            cls.wd = pathlib.Path.cwd()
        assert cls.wd.is_dir(), "faild to obtain working directory"

    @classmethod
    def tearDownClass(cls):
        # to run after all tests
        pass

    def setUp(self):
        # to run before each test
        pass

    def tearDown(self):
        # to run after each test
        pass

    def test_plotter(self):
        cfg = CfgBuilder().write_output(1).plots_close(1).build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml", verify_inputpaths=True)
        dat = load_data(cfg)
        # make a plot
        p, f = fplt.temperatures(dat, cfg)
        self.assertTrue((p is not None) and (f is not None))
        # ensure it is saved
        self.assertTrue(
            (
                pathlib.Path(cfg.params.path_output) / "diag_plots" / "Fig01_Temperatures.png"
            ).is_file()
        )
        (pathlib.Path(cfg.params.path_output) / "diag_plots" / "Fig01_Temperatures.png").unlink(
            missing_ok=False
        )

        # cleanup
        shutil.rmtree(pathlib.Path(cfg.params.path_output) / "diag_plots")

        # ensure it is closed
        self.assertFalse(p.fignum_exists(f.number))


if __name__ == "__main__":
    unittest.main()
