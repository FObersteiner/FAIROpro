# -*- coding: utf-8 -*-
import pathlib
import unittest

# import numpy as np
import pytest

from fairopro.cfg import CfgBuilder
from fairopro.data_io import load_data
from fairopro.process_cl import Flags_CL, calculate_cl_o3
from fairopro.process_uv import Flags_UV, calculate_uv_o3


class TestCfg(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            cls.wd = pathlib.Path(__file__).parent
        except NameError:
            cls.wd = pathlib.Path.cwd()
        assert cls.wd.is_dir(), "faild to obtain working directory"

    @classmethod
    def tearDownClass(cls):
        # to run after all tests
        pass

    def setUp(self):
        # to run before each test
        pass

    def tearDown(self):
        # to run after each test
        pass

    def test_flags(self):
        # ok means no bit set:
        self.assertEqual(Flags_CL.OK, 0)
        # all flags must be a power of 2 or 0
        self.assertTrue(all((int(x) & (int(x) - 1)) == 0 for x in Flags_CL))

    @pytest.mark.filterwarnings("ignore:divide by zero encountered in log")
    def test_calc(self):
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        dat = load_data(cfg)
        cfg.params.uv.f_led = 1
        cfg.params.uv.o3_zero_offset = 0
        dat, cfg = calculate_uv_o3(dat, cfg)
        dat, cfg = calculate_cl_o3(dat, cfg)
        self.assertTrue(
            (dat["cl"]["flag"] < 2 ** len(Flags_CL)).all()
        )  # flag must not exceed max flag
        self.assertTrue("cl_mr" in dat["cl"].columns)  # o3 mr is calculated

    @pytest.mark.filterwarnings("ignore:divide by zero encountered in log")
    def test_missing(self):
        # test that there is no missing data if we only select flag == 0
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        dat = load_data(cfg)
        dat, cfg = calculate_uv_o3(dat, cfg)
        dat, cfg = calculate_cl_o3(dat, cfg)
        m = dat["cl"]["flag"] == Flags_UV.OK
        s = dat["cl"]["cl_mr"].filter(m)
        # print(s.is_not_nan().all()) # FIXME: prints False in uv test!
        # self.assertTrue(np.isfinite(s.is_not_nan().to_numpy()).all())
        self.assertTrue(s.is_not_nan().all())


if __name__ == "__main__":
    unittest.main()
