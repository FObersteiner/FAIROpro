# -*- coding: utf-8 -*-
import pathlib
import unittest

# import warnings
import numpy as np
import pytest

from fairopro.cfg import CfgBuilder
from fairopro.data_io import load_data
from fairopro.process_uv import Flags_UV, calculate_uv_o3


class TestCfg(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        try:
            cls.wd = pathlib.Path(__file__).parent
        except NameError:
            cls.wd = pathlib.Path.cwd()
        assert cls.wd.is_dir(), "faild to obtain working directory"

    @classmethod
    def tearDownClass(cls):
        # to run after all tests
        pass

    def setUp(self):
        # to run before each test
        pass

    def tearDown(self):
        # to run after each test
        pass

    def test_flags(self):
        # ok means no bit set:
        self.assertEqual(Flags_UV.OK, 0)
        # all flags must be a power of 2 or 0
        self.assertTrue(all((int(x) & (int(x) - 1)) == 0 for x in Flags_UV))

    @pytest.mark.filterwarnings("ignore:divide by zero encountered in log")
    def test_calc(self):
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        dat = load_data(cfg)
        cfg.params.uv.f_led = 1
        cfg.params.uv.o3_zero_offset = 0
        dat, cfg = calculate_uv_o3(dat, cfg)
        self.assertTrue(
            (dat["uv"]["flag"] < 2 ** len(Flags_UV)).all()
        )  # flag must not exceed max flag
        self.assertTrue("mr" in dat["uv"].columns)  # o3 mr is calculated

        tmp = dat["uv"]["mr"].mean()
        cfg.params.uv.f_led = 1.95
        dat, cfg = calculate_uv_o3(dat, cfg)
        self.assertTrue(dat["uv"]["mr"].mean() < tmp)  # f_led is applied correctly

        tmp = dat["uv"]["mr"].mean()
        cfg.params.uv.o3_zero_offset = 2
        dat, cfg = calculate_uv_o3(dat, cfg)
        self.assertTrue(dat["uv"]["mr"].mean() < tmp)  # zero offset is applied correctly

        # # flag is set correctly
        w_ok = dat["uv"]["flag"] == Flags_UV.OK
        self.assertTrue(w_ok.any())
        ok_count = (dat["uv"]["flag"].filter(w_ok) == Flags_UV.OK).sum()
        # roundtrip must give equal result:
        self.assertEqual(w_ok.sum(), ok_count)

    @pytest.mark.filterwarnings("ignore:divide by zero encountered in log")
    def test_missing(self):
        # test that there is no missing data if we only select flag == 0
        cfg = CfgBuilder().verbose(False).build()
        cfg.load_params(self.wd / "testdata/cfg_with_valid_path.toml")
        dat = load_data(cfg)
        dat, cfg = calculate_uv_o3(dat, cfg)
        m = dat["uv"]["flag"] == Flags_UV.OK
        s = dat["uv"]["mr"].filter(m)
        # print(s.is_not_nan().all())  # FIXME: prints False !!
        self.assertTrue(np.isfinite(s.is_not_nan().to_numpy()).all())


if __name__ == "__main__":
    unittest.main()
