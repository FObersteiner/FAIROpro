# Changelog

<https://keepachangelog.com/>

Types of changes

- 'Added' for new features.
- 'Changed' for changes in existing functionality.
- 'Deprecated' for soon-to-be removed features.
- 'Removed' for now removed features.
- 'Fixed' for any bug fixes.
- 'Security' in case of vulnerabilities.

## [Unreleased]

## 0.4.8, 2025-02-13

### Removed

- tk dialog since unused

## 0.4.7, 2025-02-13

### Changed

- update pyproject.toml, revise package-data and build system

## 0.4.6, 2025-02-11

### Added

- V25 data tools, that were formerly part of pyfuppes
- fairopro CLI tool

### Changed

- move to uv for project management
- dependencies:
  - use pyfuppes > 0.6 from the pypi
  - use polars >= 1.4
- revised handling of parquet files; do not use pyarrow directly anymore
- revised handling of resources, i.e. default config parameters

### Removed

- citation: do not use zenodo.json anymore, cff file should be sufficient

## 0.4.5, 2024-08-06

- add pyarrow as dependency (indirectly required by polars)

## 0.4.4, 2024-07-16

- handle old CARIBIC data

## 0.4.3, 2024-06-04

- bugfix for CARIBIC FAIRO data: include 'mas' files and drop 'f_bypas' key from T_p data

## 0.4.2, 2024-05-24

- implement LOF for CL raw intensity
- adjustments to changes in polars

## 0.4.1, 2024-05-03

- add citation info
- typo in raw data to csv export function
- t-corr plot: add info about drift and SD to linear correction
- license: to GPLv3

## 0.4.0, 2024-04-09

- re-organize config file import
- CL sensitivity calculation: set NaNs to None consistency to get correct flagging
- CL downsampling: correct V25 time shift

## 0.3.14, 2024-02-22

- revise warning messages from UV and CL processing
- cfg save file / toml: correctly format 't_corr_fitparams' and 'v25_avg_t_drift' as float

## 0.3.13, 2024-02-12

- data loader: use a schema to specify dtypes for more flexibility
- add `__all__` specification to `__init__` file

## 0.3.12, 2024-02-07

- data loader: disable "truncate_ragged_lines", this is now implemented in pyfuppes/v25
- data loader: explicitly specify dtypes for all columns in all loaded file types

## 0.3.11, 2024-01-29

- data loader: set "truncate_ragged_lines" to True when reading V25 logfiles - some lines might have more fields than expected...

## 0.3.10, 2024-01-25

- cl data processing: set v25_time as "sorted" for CL downsampling

## 0.3.9, 2024-01-15

- minor fix (data import)
- now requires polars 20.4

## 0.3.8, 2023-12-18

- major revision to use polars 0.20

## 0.3.7, 2023-09-21

- use polars.Series.log instead of np.log

## 0.3.6, 2023-09-21

- numpy.log in v1.26 needs numpy arrays, not polars series

## 0.3.5, 2023-09-04

- exp_t_range is now actually implemented; if set, data is cut to the specified range (uncorrected instrument time)

## 0.3.4, 2023-08-28

- uv: thresh follow is now thresh extend mask and uses pyfuppes.filters.extend_mask

## 0.3.3, 2023-08-22

- replace `np.bitwise_or` with "normal" or; `a | b`

## 0.3.2, 2023-07-28

- show plots only after everything has been rendered
- add flagging ranges for UV LED temperature and photometer flow

## 0.3.1, 2023-06-16

- more optional output parameters for UV
- data frequency checks now only cause a warning in verbose mode

## 0.3.0, 2023-03-30

- add option to discard parts of reference time (key `cfg.params.timing.input_cut`)
- tweak timecorr plot
- fix incorrect indentation when applying 'output_cut': now all sections are masked
- introduce builder class for CFG to have a clearly defined interface for setting configurations
- make manual time correction working

## 0.2.6, 2023-03-12

- implement enum.IntFlag from the standard library for flags
- revised experiment time selection
- add `ruff` linter as dev dependency
- add pre-commit setup

## 0.2.4, 2023-03-02

- add comments to master cfg toml, one version with comments, one without
- add check if time correction was done before flagging experiment time range

## 0.2.3, 2023-02-28

- added semi-dynamic plot enumeration
- minor bugfix in setting "data loaded" state

## 0.2.2, 2023-02-23

- changed toml package, from `tomli` and `tomli-w` to `toml`
- add formatting and test of output toml files with `taplo` if available on system
- config master: key name "exp_date_utc" is now just "exp_date"

## 0.2.1, 2023-02-22

- add plots wrapper: call all plots that are adequate based on the processing config

## 0.2.0, 2023-02-20

- revised input data handling; load data from zip file and use parquet files for immediate reloading / reprocessing
- some work-arounds to handle the slightly different data from CARIBIC FAIRO

## 0.1.7, 2023-02-19

- use log cleaner from v25 module from pyfuppes package instead of binary from Rust program

## 0.1.6, 2023-02-17

- refactor: move functionality from "utils" module to more appropriate places
- revise TODOs, update README.md

## 0.1.5, 2023-02-10

- separated FAIROpro (data processing) from meta data. Meta data now in FAIROmeta, <https://git.scc.kit.edu/FObersteiner/FAIROmeta>

## 0.1.4, 2023-01-30

- add CL downsampling option / binning to lower time resolution
- add option to dump polars dataframes as parquet files, which need much less space on disk than e.g. csv files. Plus, they can be loaded to polars dataframes directly.
- cleanup: reference time parser now in corr_time module

## 0.1.2, 2023-01-27

- remove prefixes "uv*" and "cl*" from keys in uv and cl
- add option to manually invert the UV photometer signal ("invert_mr" key)
- add CL uncertainty calculation

## 0.1.1, 2023-01-27

- basic functionality complete

## 0.1.0, 2022-12-28

- revision 1 of pyFairoproc started
- new name will be FAIROpro (or fairopro as a package name)

## the past: pyFairoproc

- last version `2022.11.29a` before revision to 0.1.0 and using semver
