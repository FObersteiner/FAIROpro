# -*- coding: utf-8 -*-
"""Correct V25 timestamps"""
from datetime import datetime
from enum import IntFlag, auto

import numpy as np
import polars as pl

# handle import from same directory vs. import from module
try:
    from process_cl import Flags_CL
    from process_uv import Flags_UV
except ModuleNotFoundError:
    from fairopro.process_cl import Flags_CL
    from fairopro.process_uv import Flags_UV


# -----------------------------------------------------------------------------
TIME_UNIT = "us"
MICROSECONDS_PER_SECOND = 1e6
FITORDER = 1


class Flags_T(IntFlag):
    OK = 0
    AUTO_REMOVE = auto()
    DIFF_RANGE = auto()  # defined by cfg.params.timing.t_ref_delta_range
    REFERENCE_NOK = auto()
    MAN_REMOVE = auto()  # defined by cfg.params.timing.input_cut


# -----------------------------------------------------------------------------
# dtconfig maps file extension to
#   [0] datetime key, [1] datetime format
dtconfig = {
    "uv": ("Time", "%d.%m.%y %H:%M:%S%.f"),
    "cl": ("DateTime", "%d.%m.%y %H:%M:%S%.f"),
    "tp": ("T_pTime", "%d.%m.%y %H:%M:%S%.f"),
    "hsk": ("DateTime", "%d.%m.%y %H:%M:%S%.f"),
    "ac": ("V25_Time", "%d.%m.%y %H:%M:%S%.f"),
}


def get_tcorr_parms(t: np.ndarray, t_ref: np.ndarray, fitorder: int) -> np.ndarray:
    """Calculate fit parameters for time correction."""
    try:
        parms = np.polyfit(t, t - t_ref, fitorder)
    except np.linalg.LinAlgError:  # sometimes happens at first try...
        parms = np.polyfit(t, t - t_ref, fitorder)
    return parms


# -----------------------------------------------------------------------------
def correct_instrumenttime(dat, cfg):
    """Derive correction for V25 timestamps.

    Wraps:
        _analyze_ref_t : analyze reference time, e.g. from IWG1 recording
        _check : check cfg.params time correction settings
        _apply_corr : manifest the time correction in the data
    """
    if not cfg.params.process_t_corr:
        raise ValueError(
            "correct_instrumenttime must not be called if " "cfg.params.process_t_corr is false"
        )
    # there are two paths basically,
    #     1) use reference time
    #     2) user-defined parameters
    # for 1), preparation is needed, i.e. to calculate the correction fit function
    if cfg.params.timing.use_t_ref:
        dat, cfg = _analyze_ref_t(dat, cfg)

    # independent of 1) or 2), we need to have correction parameters at a common place
    dat, cfg = _check(dat, cfg)

    dat, cfg = _apply_corr(dat, cfg)

    cfg.params["derived"]["_proc_done"]["timecorr"] = 1

    return dat, cfg


# -----------------------------------------------------------------------------
def _analyze_ref_t(dat, cfg):
    """Analyze reference time, e.g. from IWG1 recording."""
    ref_date = datetime(*cfg.params.exp_date)
    dat["ac"] = dat["ac"].with_columns(
        (
            (
                (pl.col("ref_time") - ref_date).dt.total_microseconds() / MICROSECONDS_PER_SECOND
            ).alias("ref_mdns"),
            pl.Series([int(Flags_T.OK)] * dat["ac"].height).alias("flag"),
        )
    )
    dat["ac"] = dat["ac"].with_columns(
        (pl.col("mdns") - pl.col("ref_mdns")).alias("dt"),
    )

    # set flag where difference is out of allowed range
    dat["ac"] = dat["ac"].with_columns(
        pl.when(
            (pl.col("dt") < cfg.params.timing.t_ref_delta_range[0])
            | (pl.col("dt") >= cfg.params.timing.t_ref_delta_range[1])
        )
        .then(pl.col("flag") | Flags_T.DIFF_RANGE)
        .otherwise(pl.col("flag"))
    )

    # select data based on platform speed or instrument state
    key = None
    if cfg.params.exp_platform.upper() in ["HALO", "HIAPER", "BBFLUX"]:
        key = "T_Airspeed"
        # set flag where speed is less than 20 knts
        dat["ac"] = dat["ac"].with_columns(
            pl.when(pl.col(key).lt(10))
            .then(pl.col("flag") | Flags_T.REFERENCE_NOK)
            .otherwise(pl.col("flag"))
        )

    if cfg.params.exp_platform.upper() == "CARIBIC":
        key = "V25State"
        # set flag where state is not MS
        dat["ac"] = dat["ac"].with_columns(
            pl.when(pl.col(key).ne("MS"))
            .then(pl.col("flag") | Flags_T.REFERENCE_NOK)
            .otherwise(pl.col("flag"))
        )

    # cut-sections, where AC data should be removed / not used for fit
    if cfg.params.timing.input_cut:
        for section in cfg.params.timing.input_cut:
            m = (
                dat["ac"]
                .select((pl.col("mdns").ge(section[0])) & (pl.col("mdns").lt(section[1])))
                .to_series()
            )
            dat["ac"] = dat["ac"].with_columns(
                pl.when(m).then(pl.col("flag") | Flags_T.MAN_REMOVE).otherwise(pl.col("flag"))
            )

    # if key is still none, an error occured
    if key is None:
        raise ValueError("Could not determine key to select valid reference time")

    # check if there is remaining data (flag == 0)
    if dat["ac"].select(pl.col("flag").eq(Flags_T.OK)).to_series().sum() <= 0:
        raise ValueError(
            "Time correction: no suitable data after setting "
            "'t_ref_delta_range' and selecting data"
        )

    p = get_tcorr_parms(
        dat["ac"].select(pl.col("mdns").filter(pl.col("flag").eq(Flags_T.OK))).to_series(),
        dat["ac"].select(pl.col("ref_mdns").filter(pl.col("flag").eq(Flags_T.OK))).to_series(),
        1,
    )
    # linear fit inclination is average time drift of v25 vs. reference:
    cfg.params.derived["v25_avg_t_dirft"] = float(p[0])  # seconds per second
    # now the actual fit
    if FITORDER == 1:
        params = list(p)  # use a list; needs to be represented in cfg save
    else:
        params = list(
            get_tcorr_parms(
                dat["ac"].select(pl.col("mdns").filter(pl.col("flag").eq(Flags_T.OK))).to_series(),
                dat["ac"]
                .select(pl.col("ref_mdns").filter(pl.col("flag").eq(Flags_T.OK)))
                .to_series(),
                FITORDER,
            )
        )
    cfg.params.derived["t_corr_fitparams"] = [float(p) for p in params]  # from numpy
    cfg.params.derived["t_corr_fitorder"] = FITORDER

    return dat, cfg


# -----------------------------------------------------------------------------
def _check(dat, cfg):
    """Verify that settings defined in cfg.params make sense."""
    # If use_t_ref is false but a time correction should be applied nevertheless,
    #   we need to obtain correction parameters from settings.
    # If none are set, that should raise an error.
    if not any(
        (
            cfg.params.timing.use_t_ref,
            cfg.params.timing.t_offset_userdef,
            cfg.params.timing.t_corr_params_userdef,
        )
    ):
        raise ValueError(
            "No user-defined time correction parameters found and "
            "reference time is not used - time correction should be "
            "disabled altogether (set 'process_t_corr' to 'false')"
        )

    # If there are user-sepcified fit-parameters AND reference time was used,
    #   that should raise an error as well.
    if all(
        (
            cfg.params.timing.use_t_ref,
            cfg.params.timing.t_corr_params_userdef,
        )
    ):
        raise ValueError(
            "Cannot use both user-defined fit parameters AND fit "
            "parameters derived from reference time."
        )

    # A user-specified offset + fit parameters from reference time are fine.
    # User-specified fit parameters AND offset is also ok.
    if not cfg.params.timing.use_t_ref:
        cfg.params.derived["t_corr_fitparams"] = cfg.params.timing.t_corr_params_userdef
        if cfg.params.timing.t_corr_params_userdef:
            cfg.params.derived["t_corr_fitorder"] = len(cfg.params.timing.t_corr_params_userdef) - 1

    cfg.params.derived["t_corr_offset"] = (
        cfg.params.timing.t_offset_userdef if cfg.params.timing.t_offset_userdef else 0
    )

    return dat, cfg


# -----------------------------------------------------------------------------
def _apply_corr(dat, cfg):
    """Apply given corretion function to all dataframes in dat."""
    verboseprint = print if cfg.verbose else lambda *a, **k: None

    # make a copy of original v25_time / mdns columns, then
    # apply to all mdns columns
    # apply to all v25_time columns
    verboseprint("+++++\n" "+ time correction:")
    for k in dat:
        if dat[k] is None:
            continue  # 'ac' might be None if user-defined / manual time corr
        verboseprint(f"+   dataframe '{k}'")
        if cfg.params.derived.t_corr_fitparams:
            dat[k] = dat[k].with_columns(
                pl.Series(
                    np.polyval(cfg.params.derived.t_corr_fitparams, dat[k]["mdns"])
                    + cfg.params.derived.t_corr_offset
                ).alias("t_corr"),
            )
        else:
            dat[k] = dat[k].with_columns(
                (pl.Series([cfg.params.derived.t_corr_offset] * dat[k].height)).alias("t_corr"),
            )

        # backup copies
        dat[k] = dat[k].with_columns(
            (
                (pl.col("v25_time")).alias("v25_time_uncorr"),
                (pl.col("mdns")).alias("mdns_uncorr"),
            )
        )

        # now update columns v25_time and mdns; t_corr must be subtracted
        dat[k] = dat[k].with_columns(
            (
                pl.col("v25_time")
                - pl.duration(microseconds=pl.col("t_corr") * MICROSECONDS_PER_SECOND),
                (pl.col("mdns") - pl.col("t_corr")).alias("mdns"),
            )
        )

    return dat, cfg


# -----------------------------------------------------------------------------
def parse_reftime(cfg, df_ac, raise_error=True, _LAMBDA=18 * 60 * 60 * 1_000_000):
    """
    parse IWG1 string from aircraft, as recorded by V25

    Parameters
    ----------
    cfg : fairopro.cfg.CFG
        FAIROproc config.
    df_ac : pl.DataFrame
        dataframe with aircraft data.
    _LAMBDA : int
        filter limit for difference between V25 time and reference time.
        unit: uses the common time resolution of microseconds.

    Returns
    -------
    df_ac : pl.DataFrame
        updated dataframe, now with column "ref_time"
    """
    verboseprint = print if cfg.verbose else lambda *a, **k: None

    if cfg.params.process_t_corr and df_ac is None:  # cannot use reference data if None supplied
        if cfg.params.timing.use_t_ref and raise_error:
            raise ValueError(
                "A/C data is None, cannot obtain reference time "
                "while 'cfg.params.timing.use_t_ref' is True"
            )
        verboseprint(
            "+++++\n"
            "+ A/C timestamp parser:\n"
            "+   A/C data is None, cannot obtain reference time"
        )
        cfg.params.timing.use_t_ref = False
        return df_ac

    # find reference time key
    key = None
    for c in df_ac.columns:
        if "time" in c.lower() and "v25" not in c.lower():
            key = c
            break
    else:
        if cfg.params.timing.use_t_ref and raise_error:
            raise ValueError("Could not determine datetime column in A/C data")
        verboseprint(
            "+++++\n"
            "+ A/C timestamp parser:\n"
            "+   A/C data is None, cannot obtain reference time"
        )
        cfg.params.timing.use_t_ref = False
        return df_ac

    # shortcut: CARIBIC data is always fine NOTE : ?!
    if cfg.params.exp_platform.upper() in ["CARIBIC"]:
        df_ac = df_ac.with_columns(
            pl.col(key)
            .str.strptime(pl.Datetime, format=dtconfig["ac"][1])
            .cast(pl.Datetime(time_unit="us"))
            .alias("ref_time")
        )
        return df_ac

    # try the short route; IWG1 timestamps can be parsed
    try:
        df_ac = df_ac.with_columns(
            pl.col(key)
            .str.strptime(pl.Datetime(time_unit="us"), format=dtconfig["ac"][1])
            .alias("ref_time")
        )
        return df_ac
    except Exception as e:
        verboseprint(
            "+++++\n"
            "+ A/C timestamp parser:\n"
            f"+   encountered error: '{e}'\n"
            "+   -> proceeding with override: use V25 date where conversion failed"
        )

    # now the work begins... since we want to have a time-sync, not a date-sync,
    # use V25 date instead of IWG1 date where IWG1 datetime cannot be parsed.
    df_ac = df_ac.with_columns(
        (
            pl.col("v25_time").dt.strftime("%d.%m.%y")
            + " "
            # time is second element after split on ' ':
            + pl.col(key).str.split(by=" ").list.get(1)
        ).alias("dummy")
    )

    # a mask where strptime fails:
    m = df_ac[key].str.strptime(pl.Datetime, format=dtconfig["ac"][1], strict=False).is_null()
    # set V25 date where mask is true
    df_ac = df_ac.with_columns(
        pl.when(m).then(pl.col("dummy")).otherwise(pl.col(key)).alias("ref_time"),
    ).drop("dummy")
    df_ac = df_ac.with_columns(
        pl.col("ref_time").str.strptime(pl.Datetime(time_unit="us"), format=dtconfig["ac"][1])
    )
    # this might create large "jumps" if there is a date change.
    # remove values from the df where V25 and Ref are apart by more than 18h
    df_ac = df_ac.filter((pl.col("v25_time") - pl.col("ref_time")).cast(int).abs() < _LAMBDA)

    return df_ac


# -----------------------------------------------------------------------------


def set_range_t_exp(dat, cfg):
    """Set experiment time range by flagging values out of range as 'manually removed'."""
    verboseprint = print if cfg.verbose else lambda *a, **k: None

    # must be applied to uncorrected instrument time:
    t_key = "mdns_uncorr" if "mdns_uncorr" in dat["uv"].columns else "mdns"

    verboseprint("+++++\n" "+ set experiment time range:")
    # check if UV data was processed
    if cfg.params.derived["_proc_done"]["uv_calc"]:
        verboseprint("+   UV data")
        t_min, t_max = cfg.params.t_range_exp
        dat["uv"] = dat["uv"].with_columns(
            pl.when((pl.col(t_key) < t_min) | (pl.col(t_key) > t_max))
            .then(pl.col("flag") | Flags_UV.MAN_REMOVE)
            .otherwise(pl.col("flag"))
        )
        # UV data processed is a pre-requisite, so check if CL data processed here
        if cfg.params.derived["_proc_done"]["cl_calc"]:
            verboseprint("+   CL data")
            dat["cl"] = dat["cl"].with_columns(
                pl.when((pl.col(t_key) < t_min) | (pl.col(t_key) > t_max))
                .then(pl.col("flag") | Flags_CL.MAN_REMOVE)
                .otherwise(pl.col("flag"))
            )
    else:
        raise ValueError("At least UV data must be processed before setting experiment time range.")

    return dat, cfg
