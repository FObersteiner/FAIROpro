# -*- coding: utf-8 -*-
"""FAIROpro config handling"""
import pathlib
import subprocess
import time
from datetime import datetime, timedelta, timezone
from importlib import resources
from io import BytesIO

import pandas as pd
import toml
import yaml
from pyfuppes.misc import clean_path

# handle import from parent directory (module base)
try:
    from fairopro import __version__ as fairoprovers
except ModuleNotFoundError:
    from importlib import metadata

    fairoprovers = metadata.version("fairopro")

try:
    wd = pathlib.Path(__file__).parent
except NameError:
    wd = pathlib.Path.cwd()  # same as os.getcwd()
assert wd.is_dir(), "faild to obtain working directory"


with (resources.files("fairopro") / "resources/master_cfg.toml").open("rb") as f:
    master_cfg = f.read()
with (resources.files("fairopro") / "resources/cfg_keys.ods").open("rb") as f:
    master_cfg_spreadsheet = f.read()


# -----------------------------------------------------------------------------

# FAIRO platforms
INSTR_PLATFORMS = ("caribic", "halo", "hiaper", "unknown")

# required keys in config
KEYS_ESS = ["exp_platform", "path_input", "path_output"]


# -----------------------------------------------------------------------------


class dotdict(dict):
    """Wrapper class around dict for dot-notation access to dictionary keys."""

    __getattr__ = dict.get
    __setattr__ = dict.__setitem__  # type: ignore
    __delattr__ = dict.__delitem__  # type: ignore


# -----------------------------------------------------------------------------


class CfgBuilder:
    """A class to create a FAIROpro cfg"""

    def __init__(self):
        self.cfg = CFG()

    def verbose(self, value):
        """verbose print output"""
        self.cfg._verbose = value
        return self

    def force_load_from_raw(self, value):
        """always load data from the zip file, not parquet files"""
        self.cfg._force_load_from_raw = value
        return self

    def write_raw_merge_csv(self, value):
        """create csv files with merged data from V25 logfiles"""
        self.cfg._write_raw_merge_csv = value
        return self

    def write_output(self, value):
        """write output or dry run"""
        self.cfg._write_output = value
        return self

    def write_params_uv(self, value):
        """which params to write for UV data, to text file output"""
        self.cfg._write_params_uv = value
        return self

    def write_params_cl(self, value):
        """which params to write for CL data, to text file output"""
        self.cfg._write_params_cl = value
        return self

    def write_df_dump(self, value):
        """dump dataframes to parquet files"""
        self.cfg._write_df_dump = value
        return self

    def plots_close(self, value):
        """close the plots after creating and saving them"""
        self.cfg._plots_close = value
        return self

    def time_format(self, value):
        """datetime format in text file output, seconds after midnight or ISO 8601"""
        if value not in ("mdns", "iso"):
            raise ValueError(f"value must be 'mdns' or 'iso', got '{value}'")
        self.cfg._time_format = value
        return self

    def output_float_precision(self, value):
        """number of decimal places in text file output"""
        if value is not None and not isinstance(value, int) and not value > 0:
            raise ValueError(f"value must be a positive integer or None, got '{value}'")
        self.cfg._output_float_precision = value  # type: ignore
        return self

    def build(self):
        """create the config file"""
        return self.cfg


class CFG:
    """A class to provide FAIRO data processing configuration."""

    def __init__(self):
        # I) general attributes
        self._verbose = 0
        self._force_load_from_raw = 1  # forces (re-)loading the data from YYYY-MM-DD.zip
        self._write_raw_merge_csv = 0
        self._write_output = 0  # default to dry run
        self._write_params_uv = ["O3_ppb"]  # O3_ppb is an alias for mr
        self._write_params_cl = ["O3_ppb"]  # O3_ppb is an alias for cl_mr
        self._write_df_dump = 0  # {0, "csv", "parquet"}
        self._plots_close = 0

        self._time_format = "mdns"  # seconds after midnight or "iso" for ISO8601
        self._output_float_precision = None
        # II) experiment-specific attributes are stored in a private dict
        self._params = dotdict({})

    @property
    def verbose(self):
        return self._verbose

    @property
    def force_load_from_raw(self):
        return self._force_load_from_raw

    @property
    def write_raw_merge_csv(self):
        return self._write_raw_merge_csv

    @property
    def write_output(self):
        return self._write_output

    @property
    def write_params_uv(self):
        return self._write_params_uv

    @property
    def write_params_cl(self):
        return self._write_params_cl

    @property
    def write_df_dump(self):
        return self._write_df_dump

    @property
    def plots_close(self):
        return self._plots_close

    @property
    def time_format(self):
        return self._time_format

    @property
    def output_float_precision(self):
        return self._output_float_precision

    @property
    def params(self):
        return self._params

    def load_params(
        self,
        filepath: pathlib.Path,
        verify_inputpaths: bool = True,
        is_legacy: bool = False,
    ):
        """Load processing parameters for one experiment from a toml file."""
        if not isinstance(filepath, pathlib.Path):
            filepath = pathlib.Path(filepath).resolve()
        new_path = filepath
        if is_legacy:
            new_path = filepath.parent / (filepath.stem + "_converted.toml")
            convert_legacy_cfg(filepath, new_path)
        dict_from_toml = load_cfg(new_path, verify_inputpaths=verify_inputpaths)
        self._params = dotdict(dict_from_toml)

    def save_params(self) -> pathlib.Path:
        return save_cfg(self._params)

    def __repr__(self):
        r = (
            f"FAIROpro v{fairoprovers} config",
            "+++ io settings +++++++",
            f" verbose: {str(self.verbose)}",
            f" force_load_from_raw: {str(self.force_load_from_raw)}",
            f" write_raw_merge: {str(self._write_raw_merge_csv)}",
            f" write_output: {str(self.write_output)}",
            f" write_params_uv: {str(self.write_params_uv)}",
            f" write_params_cl: {str(self.write_params_cl)}",
            f" output_float_precision: {self.output_float_precision}",
            f" plots_close: {str(self.plots_close)}",
            "+++ params ++++++++++++\n",
        )
        return "\n".join(r) + _print_nested_dict(self.params)

    def __str__(self):
        s = (
            f"FAIROpro v{fairoprovers} config",
            "---------------------------",
            f"verbose               : {'on' if self.verbose else 'off'}",
            f"force load zipfile    : {'yes' if self.force_load_from_raw else 'no'}",
            f"write merge-csv       : {'yes' if self.write_raw_merge_csv else 'no'}",
            f"write output          : {'yes' if self.write_output else 'no'}",
            f"output parameters, UV : {', '.join(self.write_params_uv)}",
            f"output parameters, CL : {', '.join(self.write_params_cl)}",
            f"close plots           : {'yes' if self.plots_close else 'no'}",
        )
        return "\n".join(s)


# -----------------------------------------------------------------------------


def load_cfg(
    filepath: pathlib.Path,
    verify_inputpaths: bool = True,
) -> dict:
    """Load a FAIROpro settings file from specified path."""

    with open(filepath, "r", encoding="UTF-8") as fp:
        settings = toml.load(fp)

    settings["derived"] = dotdict()  # this is used for output
    # if it is part of loaded settings, it will be overwritten by this

    # verify that essential keys are provided
    for key in KEYS_ESS:  # check if essential parameters are given
        if key not in settings:
            raise ValueError(f"missing essential key '{key}'")

    assert (
        settings["exp_platform"].lower() in INSTR_PLATFORMS
    ), f"platform {settings['exp_platform']} not found in {INSTR_PLATFORMS}"

    # load the master settings file and transfer any defaults that are missing
    # in the loaded file
    master_settings = toml.loads(master_cfg.decode("utf-8"))

    for k, v in master_settings.items():
        if k in KEYS_ESS:  # essential keys are all top-level
            continue
        if (have := settings.get(k)) is None:
            # key does not exist at all, so create it with defaults
            settings[k] = dotdict(v) if isinstance(v, dict) else v
        elif isinstance(have, dict):  # group key, analyse group
            for subk, subv in v.items():
                if subk not in have:
                    settings[k][subk] = subv
        # else: implicit, key is set

    # remove params_file key, might be there if saved cfg is used
    settings.pop("params_file", None)
    settings["derived"]["params_file"] = filepath.resolve().as_posix()

    # state container
    settings["derived"]["_proc_done"] = {
        "loaded_from_raw": 0,  # implicit: cleaned
        "loaded_from_pq": 0,
        "uv_calc": 0,
        "cl_calc": 0,
        "timecorr": 0,
    }

    # shoud catch invalid input paths here. also, convert to full path if relative
    if verify_inputpaths:
        _p = pathlib.Path(settings["path_input"])
        if not _p.is_absolute():  # we have a relative path...
            _ps = _p.as_posix()
            if _ps.startswith("$HOME") or _ps.startswith("~"):  # must replace these 'manually'...
                settings["path_input"] = clean_path(_ps, resolve=True)
            else:
                settings["path_input"] = (
                    pathlib.Path(settings["derived"]["params_file"]).parent / _p
                ).resolve()
        else:  # we have an absolute path, just ensure it's a pathlib.Path object
            settings["path_input"] = _p

        if not settings["path_input"].is_dir():
            raise ValueError(f"'{settings['path_input']}' is not a valid directory")

    # if path_output is relative, it should be relative to the path of the settings file
    _p = pathlib.Path(settings["path_output"])
    if _p.is_absolute():
        settings["path_output"] = _p
    else:
        _ps = _p.as_posix()
        if _ps.startswith("$HOME") or _ps.startswith("~"):
            settings["path_output"] = clean_path(_ps, resolve=True)
        else:
            settings["path_output"] = (
                pathlib.Path(settings["derived"]["params_file"]).parent / _p
            ).resolve()

    # sort and convert groups to dot-dicts
    settings = dict(sorted(settings.items()))
    for k, v in settings.items():
        if isinstance(v, dict):
            settings[k] = dotdict(sorted(v.items()))

    settings["derived"]["cfg_loaded_time"] = datetime.now(timezone.utc)

    return settings


# -----------------------------------------------------------------------------
def convert_legacy_cfg(
    src: pathlib.Path,
    dst: pathlib.Path,
    verify_inputpaths: bool = False,
) -> dict:
    """Convert old pyFairoproc yaml settings file to FAIROpro-type toml file."""
    assert src.is_file(), f"{str(src)} is not a valid input"

    df = pd.read_excel(BytesIO(master_cfg_spreadsheet), skiprows=1).set_index("old_key_name")

    with open(src, "r") as fp:
        cfg_old = yaml.load(fp, Loader=yaml.FullLoader)

    # remove keys that have None value
    cfg_old = {k: v for k, v in cfg_old.items() if v is not None}

    # OMCoutput_cut and OSCoutput_cut need special treatment
    for k in "OMCoutput_cut", "OSCoutput_cut":
        if isinstance(arr := cfg_old.get(k), list):  # key must be list
            cfg_old[k] = [[arr[i + 1], arr[i + 2]] for i in range(arr[0])]

    cfg_new = {
        g: {} for g in df["group"].unique() if g not in ("-", "top-level")
    }  # group: {key: default}

    for k, v in cfg_old.items():
        row = df.iloc[df.index.get_loc(k)]
        if "remove" in row.change_notes:
            print(f"skipped key '{k}' since not used anymore")
            continue
        if row["group"] == "top-level":
            cfg_new[row.new_key_name] = v
        else:
            cfg_new[row["group"]][row["new_key_name"]] = v

    # new config file format only specifies one input paths, where to find the zipfile:
    cfg_new["path_input"] = pathlib.Path(cfg_new["path_input"][0]).parent.as_posix()  # type: ignore

    if verify_inputpaths:
        if not pathlib.Path(cfg_new["path_input"]).is_absolute():
            cfg_new["path_input"] = (  # type: ignore
                # pathlib.Path(cfg_new["derived"]["params_file"]).parent
                src.parent
                / cfg_new["path_input"]
            ).resolve()
        else:
            cfg_new["path_input"] = pathlib.Path(cfg_new["path_input"])  # type: ignore
        if not cfg_new["path_input"].is_dir():
            raise ValueError(f"'{cfg_new['path_input']}' is not a valid directory")
        # back to string so this is toml-serializable:
        cfg_new["path_input"] = cfg_new["path_input"].as_posix()  # type: ignore

        # 2 - output path
        p = cfg_new["path_output"]
        if not pathlib.Path(p).is_absolute():  # type: ignore
            cfg_new["path_output"] = (pathlib.Path(src).parent / p).resolve().as_posix()  # type: ignore
        if not pathlib.Path(cfg_new["path_output"]).is_dir():  # type: ignore
            raise ValueError(f"'{cfg_new['path_output']}' is not a valid directory")

    cfg_new = dict(sorted(cfg_new.items()))
    for k, v in cfg_new.items():
        if isinstance(v, dict):
            cfg_new[k] = dotdict(sorted(v.items()))

    header = [
        "# FAIROpro config parameters",
        "# converted from legacy format",
        f"# input file: {str(src)}",
        f"# saved {datetime.now(tz=timezone.utc).isoformat(timespec='milliseconds')}",
        f"# fairopro package version: {fairoprovers}",
        "# ***",
        "# [group]",
        "# KEY: Parameter",
        "# ***",
    ]

    with open(dst, "w", encoding="UTF-8") as fp:
        fp.write("\n".join(header) + "\n")

    with open(dst, "a") as fp:
        toml.dump(cfg_new, fp)

    _call_taplo_fmt(dst.as_posix())

    return cfg_new


# -----------------------------------------------------------------------------
def save_cfg(settings) -> pathlib.Path:
    """Save FAIROpro settings to toml file."""

    # make a deep copy by sorting to a new nested dict
    settings = dict(sorted(settings.items()))
    for k, v in settings.items():
        if isinstance(v, dict):
            settings[k] = dict(sorted(v.items()))

    # drop state; this is only relevant while running FAIROpro
    settings["derived"].pop("_proc_done")

    # convert paths to string to enable re-import as list
    settings["path_input"] = pathlib.Path(settings["path_input"]).as_posix()
    settings["path_output"] = pathlib.Path(settings["path_output"]).as_posix()

    # a precaution to prevent the same filename generated twice:
    if datetime.now(tz=timezone.utc) - settings["derived"]["cfg_loaded_time"] < timedelta(
        seconds=1
    ):
        time.sleep(1)

    settings["derived"]["cfg_loaded_time"] = settings["derived"]["cfg_loaded_time"].isoformat(
        timespec="milliseconds"
    )

    # try to use the same timestamp for cfg filename as used for data
    if (t := settings["derived"].get("data_saved_at")) is None:
        t = datetime.now(tz=timezone.utc).strftime("%Y%m%dT%H%M%SZ")
    else:
        t = datetime.fromisoformat(t).strftime("%Y%m%dT%H%M%SZ")

    fname = pathlib.Path(settings["path_output"]) / "cfg_save" / f"FAIROpro_params_{t}.toml"

    if not fname.parents[0].exists():
        fname.parents[0].mkdir()

    header = [
        "# FAIROpro config parameters log",
        f"# saved {datetime.now(tz=timezone.utc).isoformat(timespec='milliseconds')}",
        f"# FAIROpro package version: {fairoprovers}",
        "# ***",
        "# [group]",
        "# KEY: Parameter",
        "# ***",
    ]

    with open(fname, "w", encoding="UTF-8") as fp:
        fp.write("\n".join(header) + "\n")

    with open(fname, "a", encoding="UTF-8") as fp:
        toml.dump(settings, fp)

    _call_taplo_fmt(fname.as_posix(), hide_output=True)

    return fname


# -----------------------------------------------------------------------------
def _print_nested_dict(d: dict) -> str:
    """Helper for the CFG class repr."""
    result = []
    ds = dict(sorted(d.items()))
    ds = dict(sorted(ds.items(), key=lambda kv: isinstance(kv[1], dict)))
    for k, v in ds.items():
        if isinstance(v, dict):
            ds[k] = dict(sorted(v.items()))
            result.append(f" [{k}]")
            for sk, sv in ds[k].items():
                result.append(f"   {sk} : {sv}")
        else:
            result.append(f" {k} : {v}")
    return "\n".join(result)


# -----------------------------------------------------------------------------
def _call_taplo_fmt(fname: str | pathlib.Path, hide_output=False):
    """
    verify and format with taplo if available
    https://taplo.tamasfe.dev/
    """
    try:
        _ = subprocess.run(
            f"taplo fmt {fname} -- --test",
            shell=True,
            check=True,
            capture_output=hide_output,
        )
    except subprocess.CalledProcessError as e:
        print("missing taplo library, toml not re-formatted;")
        print(e)
