# -*- coding: utf-8 -*-
"""Chemiluminescence detector calculations"""
from enum import IntFlag, auto

import polars as pl
from pyfuppes.filters import mask_jumps, simple_1d_lof
from pyfuppes.interpolate import pl_Series_interp1d as interp1d
from scipy.ndimage import median_filter

# smoothing window (UV sampling scale) for chemiluminescence sensitivity
CL_SENSI_SMOOTH_WIN = 0
# ... and pressure difference over the CL detector
CL_DP_SMOOTH_WIN = 6


# -----------------------------------------------------------------------------
class Flags_CL(IntFlag):
    OK = 0
    AUTO_REMOVE = auto()
    O3_RANGE = auto()
    I_RANGE = auto()
    I_JUMP = auto()
    I_LOF = auto()
    P_RANGE = auto()
    S_RANGE = auto()
    MAN_REMOVE = auto()


# -----------------------------------------------------------------------------
def calculate_cl_o3(dat, cfg):
    """Calculate ozone mixing ratio with UV photometer data.

    Wraps
        _prepare : get the necessary data
        _calibrate : calculate sensitivity
        _post_process : calculate the cl o3 mixing ratio, apply thresholds,
                        calculate uncertainty,
                        optionally downsample the data to another frequency
    """
    dat, cfg = _prepare(dat, cfg)

    dat, cfg = _calibrate(dat, cfg, CL_SENSI_SMOOTH_WIN, CL_DP_SMOOTH_WIN)

    dat, cfg = _post_process(dat, cfg)

    cfg.params["derived"]["_proc_done"]["cl_calc"] = 1

    return dat, cfg


# -----------------------------------------------------------------------------
def _prepare(dat, cfg):
    """Interpolations etc."""

    verboseprint = print if cfg.verbose else lambda *a, **k: None

    # flag values where state is not cfg.params.coerce_state
    t = (
        dat["uv"]
        .filter(pl.col("State") == cfg.params.state_coerce)
        .select(["v25_time"])
        .to_series()
    )
    dat["cl"] = dat["cl"].with_columns(
        ((pl.col("v25_time") < t.min()) & (pl.col("v25_time") < t.max())).alias("flag").cast(int)
    )

    # apply intensity thresholds
    dat["cl"] = dat["cl"].with_columns(
        pl.when(
            (pl.col("I") < cfg.params.cl.range_int[0]) | (pl.col("I") >= cfg.params.cl.range_int[1])
        )
        .then(pl.col("flag") | Flags_CL.I_RANGE)
        .otherwise(pl.col("flag"))
    )

    # mask/filter jumps
    if cfg.params.cl.filter_jumps:
        dat["cl"] = dat["cl"].with_columns(
            pl.when(
                pl.Series(
                    ~mask_jumps(
                        dat["cl"]["I"].to_numpy(),
                        cfg.params.cl.int_maxjump,
                        1,
                        use_abs_delta=False,  # positive jumps only
                    )
                )
            )
            .then(pl.col("flag") | Flags_CL.I_JUMP)
            .otherwise(pl.col("flag"))
        )

    # local outlier factor filter
    if cfg.params.cl.lof_filter:
        #
        dat["cl"] = dat["cl"].with_columns(
            pl.when(
                pl.Series(
                    # mask from lof
                    simple_1d_lof(
                        dat["cl"]["I"].to_numpy(),
                        cfg.params.cl.lof_filter_n,
                        cfg.params.cl.lof_filter_thresh,
                        mode="positive",
                    )
                )
            )
            .then(pl.col("flag") | Flags_CL.I_LOF)
            .otherwise(pl.col("flag"))
        )

    # cut-sections, where CL data should be removed from output
    if cfg.params.cl.output_cut:
        for section in cfg.params.cl.output_cut:
            m = (
                dat["cl"]
                .select((pl.col("mdns") >= section[0]) & (pl.col("mdns") < section[1]))
                .to_series()
            )
            if m.any():
                dat["cl"] = dat["cl"].with_columns(
                    pl.when(m).then(pl.col("flag") | Flags_CL.MAN_REMOVE).otherwise(pl.col("flag"))
                )

    # we need p_preOSC and dp_OSCAR interpolated from tp data
    mapping = {"p_preOSC": "tp_ppre_cl", "dp_Oscar": "tp_dp_cl"}
    for src_name, dst_name in mapping.items():
        dat["cl"] = interp1d(
            dat["tp"],
            dat["cl"],
            ivar_src_name="v25_time",
            ivar_dst_name="v25_time",
            dvar_src_name=src_name,
            dvar_dst_name=dst_name,
            kind="linear",
            bounds_error=False,
            fill_value=None,
        )

    # apply pressure thresholds, p_preOSC and dp_OSC
    dat["cl"] = dat["cl"].with_columns(
        pl.when(
            (pl.col("tp_ppre_cl") < cfg.params.cl.range_p[0])
            | (pl.col("tp_ppre_cl") >= cfg.params.cl.range_p[1])
        )
        .then(pl.col("flag") | Flags_CL.P_RANGE)
        .otherwise(pl.col("flag"))
    )
    dat["cl"] = dat["cl"].with_columns(
        pl.when(
            (pl.col("tp_dp_cl") < cfg.params.cl.range_dp[0])
            | (pl.col("tp_dp_cl") >= cfg.params.cl.range_dp[1])
        )
        .then(pl.col("flag") | Flags_CL.P_RANGE)
        .otherwise(pl.col("flag"))
    )

    # normalize I by dividing it by p; p = ppre-dp
    dat["cl"] = dat["cl"].with_columns(
        (pl.col("I") / (pl.col("tp_ppre_cl") - pl.col("tp_dp_cl"))).alias("I_norm")
    )
    # I_norm must be null where flag is not 0 (nok data), since it is used for S calculation
    dat["cl"] = dat["cl"].with_columns(
        pl.when(pl.col("flag").eq(Flags_CL.OK)).then(pl.col("I_norm")).otherwise(None)
    )

    # check time resolution
    avg_period = (
        dat["cl"]
        .select(pl.col("mdns").filter(pl.col("flag").eq(Flags_CL.OK)).diff())
        .to_series()
        .mean()
    )
    if avg_period is None:
        raise ValueError(
            "Could not deterime CL data frequency. Please make sure to load a valid data set."
        )
    cfg.params.derived["cl_freq_have"] = 1 / avg_period

    if abs(cfg.params.derived["cl_freq_have"] - cfg.params.cl.freq) / cfg.params.cl.freq > 0.1:
        verboseprint(
            "+++++\n"
            "+ Warning:\n"
            "+   CL data frequency deviates from predefined frequncy by more than 10%\n"
            f"+    -> have: {cfg.params.derived['cl_freq_have']:.4f}, want: {cfg.params.cl.freq}"
        )

    cfg.params.derived["tp_freq_have"] = 1 / dat["tp"]["mdns"].diff().mean()
    if cfg.params.derived["tp_freq_have"] is None:
        raise ValueError(
            "Could not deterime Tp data frequency. Please make sure to load a valid data set."
        )

    return dat, cfg


# -----------------------------------------------------------------------------
def _calibrate(dat, cfg, _s_smooth_window: int = 0, _dp_smooth_window: int = 6):
    """Calculate sensitivity."""

    # 1. interpolate I_norm to uv dataframe on newly created datetime axis
    dat["uv"] = interp1d(
        dat["cl"],
        dat["uv"],
        ivar_src_name="v25_time",
        ivar_dst_name="v25_time",
        dvar_src_name="I_norm",
        dvar_dst_name="cl_I_norm",
        kind="linear",
        bounds_error=False,
        fill_value=None,
    )

    # 2. we also need uv mr only where it is valid
    dat["uv"] = dat["uv"].with_columns(
        pl.when(pl.col("flag").eq(0)).then(pl.col("mr")).otherwise(None).alias("mr_tmp")
    )

    # 3. calculate raw S, S = I_norm/MR_uv
    #    interpolate linearly to fill missing data.
    dat["uv"] = dat["uv"].with_columns(
        (pl.col("cl_I_norm") / pl.col("mr_tmp"))
        .fill_nan(None)
        .interpolate(method="linear")
        .alias("cl_S_raw")
    )

    # 4. apply median filter to remove dips and glitches
    window_sz = int(cfg.params.cl.sensi_avg_win * cfg.params.derived.uv_freq_have)
    window_sz += 1 if window_sz % 2 else window_sz
    dat["uv"] = dat["uv"].with_columns(
        pl.Series((median_filter(dat["uv"]["cl_S_raw"], window_sz)))
        .fill_nan(None)
        .alias("cl_S_filtered")
    )
    if _s_smooth_window:  # is not None or zero
        # apply slight smoothing to filtered sensitivity (moving avg)
        dat["uv"] = dat["uv"].with_columns(
            pl.col("cl_S_filtered")
            .fill_nan(None)
            .rolling_mean(window_size=_s_smooth_window)
            .shift(int(-_s_smooth_window / 2))
            .alias("cl_S_smooth")
        )
    else:
        dat["uv"] = dat["uv"].with_columns(
            pl.col("cl_S_filtered").fill_nan(None).alias("cl_S_smooth")
        )

    # 6. interpolate S back to cl time axis
    dat["cl"] = interp1d(
        dat["uv"],
        dat["cl"],
        ivar_src_name="v25_time",
        ivar_dst_name="v25_time",
        dvar_src_name="cl_S_smooth",
        dvar_dst_name="S_smooth",
        kind="linear",  # no effect with pl_Series_ip1d_lite
        bounds_error=False,  # no effect with pl_Series_ip1d_lite
        fill_value=None,
    )

    # 7. account for dp OSC
    #    we average dp alongside instantaneous dp
    window_sz = int(
        _dp_smooth_window
        * (cfg.params.cl.sensi_avg_win * cfg.params.derived.cl_freq_have)
        / (cfg.params.cl.sensi_avg_win * cfg.params.derived.uv_freq_have)
    )
    window_sz += 1 if window_sz % 2 else window_sz
    dat["cl"] = dat["cl"].with_columns(
        pl.col("tp_dp_cl")
        .fill_nan(None)
        .rolling_mean(window_size=window_sz)
        .shift(int(-window_sz / 2))
        .alias("tp_dp_cl_avg")
    )
    # now we can apply a dp correction (flow) to S:
    dat["cl"] = dat["cl"].with_columns(
        (
            (
                (pl.col("tp_dp_cl") ** 0.2 / pl.col("tp_dp_cl_avg") ** 0.2) * pl.col("S_smooth")
            ).fill_nan(None)
        ).alias("S_corr")
    )

    return dat, cfg


# -----------------------------------------------------------------------------
def _post_process(dat, cfg):
    """Clean-up etc."""

    # apply S threshold (invalid data is None by now)
    dat["cl"] = dat["cl"].with_columns(
        pl.when(
            (pl.col("S_corr") < cfg.params.cl.range_sensi[0])
            | (pl.col("S_corr") >= cfg.params.cl.range_sensi[1])
        )
        .then(pl.col("flag") | Flags_CL.S_RANGE)
        .otherwise(pl.col("flag"))
    )

    # calculat CL MR as I_norm / S_corr
    dat["cl"] = dat["cl"].with_columns(
        (pl.col("I_norm") / pl.col("S_corr")).alias("cl_mr").fill_nan(None)
    )

    # interpolate this to uv timescale as well
    # TODO: should this be averaging instead of interpolation? binning to UV sampling interval?
    dat["uv"] = interp1d(
        dat["cl"],
        dat["uv"],
        ivar_src_name="v25_time",
        ivar_dst_name="v25_time",
        dvar_src_name="cl_mr",
        dvar_dst_name="cl_mr",
        kind="linear",
        bounds_error=False,
        fill_value=None,
    )

    # apply o3 range limits
    if cfg.params.uv.o3_range:
        dat["cl"] = dat["cl"].with_columns(
            pl.when(
                (pl.col("cl_mr") < cfg.params.uv.o3_range[0])
                | (pl.col("cl_mr") >= cfg.params.uv.o3_range[1])
            )
            .then(pl.col("flag") | Flags_CL.O3_RANGE)
            .otherwise(pl.col("flag"))
        )

    # calculate uncertainty; cfg.params.cl.unc_rel and cfg.params.cl.unc_abs_min
    # use what ever is larger for col mr_unc_abs
    dat["cl"] = dat["cl"].with_columns(
        pl.when(pl.col("cl_mr") * cfg.params.cl.unc_rel < cfg.params.cl.unc_abs_min)
        .then(cfg.params.cl.unc_abs_min)
        .otherwise(pl.col("cl_mr") * cfg.params.cl.unc_rel)
        .alias("cl_mr_unc_abs")
    )
    # only where flag = 0
    dat["cl"] = dat["cl"].with_columns(
        pl.when(pl.col("flag").eq(Flags_CL.OK)).then(pl.col("cl_mr_unc_abs")).otherwise(None)
    )

    # downsample to lower resolution by binning (arith. mean per bin) if configured
    #   - creates a new dataframe, dat["cl_downsam"]
    # Note: mdns are also binned, so they then represent bin mean time.
    #       In contrast, v25_time represents bin start time.
    if cfg.params.cl.downsample_freq:
        # convert specified frequency to period with milliseconds resolution
        period = f"{int((1/cfg.params.cl.downsample_freq) * 1000)}ms"
        dat["cl"] = dat["cl"].with_columns(pl.col("v25_time").set_sorted())
        dat["cl_downsam"] = (
            dat["cl"].group_by_dynamic("v25_time", every=period).agg(pl.exclude("v25_time").mean())
        )

        # since v25_time is excluded from 'mean()', it will lag behind by by 1/2 period.
        # Account for that by adding half the down-sampling period.
        dat["cl_downsam"] = dat["cl_downsam"].with_columns(
            (
                pl.col("v25_time")
                + (pl.duration(milliseconds=int((1 / cfg.params.cl.downsample_freq) * 500)))
            )
        )

    # from matplotlib import pyplot as plt
    # fig, ax = plt.subplots()
    # ax1 = ax.twinx()
    # ax.plot(dat["cl"]["mdns"], dat["cl"]["tp_dp_cl"], "r", label="dp")
    # ax.plot(dat["cl"]["mdns"], dat["cl"]["tp_dp_cl_avg"], "g", label="dp_avg")
    # ax1.plot(dat["cl"]["mdns"], dat["cl"]["S_smooth"], "b", label="S_smooth")
    # ax1.plot(dat["cl"]["mdns"], dat["cl"]["S_corr"], "cyan", label="S_corr")
    # ax.plot(dat["cl"]["mdns"], dat["cl"]["cl_mr"], "k", label="cl_mr")
    # ax.plot(dat["uv"]["mdns"], dat["uv"]["mr"], "dodgerblue", marker="o", label="uv_mr")
    # fig.legend()
    # plt.show()

    return dat, cfg
