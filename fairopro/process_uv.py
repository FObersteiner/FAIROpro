# -*- coding: utf-8 -*-
"""
UV photometer calculations

To calculate the ozone mixing ratio from the 2-channel signals, a vectorized
version of "Technik D" as described in ZahnWeppner_Berechnung_O3-MR.pdf is used.
"""
import math
from enum import IntFlag, auto

import polars as pl
from pyfuppes.filters import extend_mask
from pyfuppes.interpolate import pl_Series_interp1d as interp1d

# -----------------------------------------------------------------------------
# constants
P0 = 1013.25  # hPa
T0 = 273.15  # K
L = 0.3784  # m # cuvette length
N = 2.686780111e25  # m^-3 # Loschmidt number

# O3 absorption cross section, m^2 / molecule, at STP
# SIGMA = 1.147e-21  # Hearn 1961, DOI: 10.1088/0370-1328/78/5/340
SIGMA = 1.136e-21  # Barnes & Mauersberger 1987, DOI: 10.1029/JD092iD12p14861
# SIGMA = 1.132935e-21 # Hodges et al. 2019, DOI: 10.1088/1681-7575/ab0bdd

# Temperature correction of the O3 cross section
TCORRFITPARMS = [
    3.09e-3,
    -6.63e-5,
    -3.88e-7,
    -3.18e-8,
    -4.01e-10,
]  # also Barnes & Mauersberger 1987


# "flag" (binary)
#      flag = 00000000 => ok
#      flag = 00000001 => automatically excluded (instrument not in MS etc.)
#      flag = 00000010 => out of specified O3 range
#      flag = 00000100 => ran into noise thresh
#      flag = 00001000 => etc.
#
# ...'or' them together, e.g. "O3 MR out of range" and "noise thresh exceeded"
# would be 00000010 | 00000100 = 00000110 (2 | 4 = 6)
class Flags_UV(IntFlag):
    OK = 0
    AUTO_REMOVE = auto()
    O3_RANGE = auto()
    NOISE_THRESH = auto()
    FLOW_RANGE = auto()
    LED_T_RANGE = auto()
    MAN_REMOVE = auto()


# -----------------------------------------------------------------------------
def calculate_uv_o3(dat, cfg):
    """Calculate ozone mixing ratio with UV photometer data.

    Wraps
        _prepare : prepare mixing ratio calculation (interpolate data from Tp, etc.)
        _calc_o3 : calculate mixing ratio
        _post_process : remove helper columns, set flags,
                        calculate uncertainty, etc.
    """
    dat, cfg = _prepare(dat, cfg)

    dat, cfg = _calc_o3(dat, cfg)

    dat, cfg = _post_process(dat, cfg)

    cfg.params["derived"]["_proc_done"]["uv_calc"] = 1

    return dat, cfg


# -----------------------------------------------------------------------------
def _prepare(dat, cfg):
    """Checks, interpolations etc."""
    verboseprint = print if cfg.verbose else lambda *a, **k: None

    # if cfg.params.exp_platform.upper() == "CARIBIC":
    #     # p cuvette sensor correction for CARIBIC, see experiments 2024-05-29, 2024-06-02
    #     def p_corr_caribicO3(p: float) -> float:
    #         """Empirical correction for the CARIBIC O3 cuvette pressure sensor"""
    #         FITP_LOW = [0.01850328, -0.55854629]
    #         FITP_HIGH = [-5.87565119e-02, 5.95780011e01]
    #         FIT_CROSS = 778.3680098127012
    #         if (p < 0) or (p > 1010):
    #             return p
    #         f = np.poly1d(FITP_LOW) if (p <= FIT_CROSS) else np.poly1d(FITP_HIGH)
    #         return p - f(p)
    #
    #     dat["tp"] = dat["tp"].with_columns(
    #         pl.col("p_Cuev").map_elements(p_corr_caribicO3, return_dtype=pl.Float64)
    #     )

    # old O3 data does not have median intensity (all null). use mean as a placeholder.
    if dat["uv"].select(pl.col("I_Med_C1").is_null()).to_series().all():
        dat["uv"] = dat["uv"].with_columns(pl.col("I_C1").alias("I_Med_C1"))
    if dat["uv"].select(pl.col("I_Med_C2").is_null()).to_series().all():
        dat["uv"] = dat["uv"].with_columns(pl.col("I_C2").alias("I_Med_C2"))

    # Use values where instrument state is MS and measurements are
    # alternating between the two cuvettes (flag = 0 (ok)).
    # Otherwise, flag is set to 1 (excluded automatically).
    state_invalid = dat["uv"].select(pl.col("State").ne(cfg.params.state_coerce)).to_series()
    cv_equal = (
        dat["uv"]
        .select(pl.col("Cuevette").eq(pl.col("Cuevette").shift(1)))
        .fill_null(True)
        .to_series()
    )

    dat["uv"] = dat["uv"].with_columns((state_invalid | cv_equal).alias("flag").cast(int))

    n_dropped = (~state_invalid & cv_equal).sum()  # state valid but cuvettes equal
    if n_dropped > 0:
        verboseprint(
            "+++++\n"
            "+ UV data processing:\n"
            f"+   dropped {n_dropped} measurement(s) with "
            f"state {cfg.params.state_coerce}\n+   but measurements not "
            "alternating between the two cuvettes"
        )

    # INTERPOLATE Tp data to UV timescale
    # we need temperature and cuvette pressure interpolated from tp data
    keys = {"T": None, "p": {"p_Cuev": "tp_p_cv"}, "F": {"f_OMCAL": "tp_F_photom"}}
    # Temperature needs a common name, independent of what sensor is used
    if cfg.params.uv.temperature_pos == "inside":
        keys["T"] = {
            "T_C1_In": "tp_T_cv1",
            "T_C2_In": "tp_T_cv2",
            "T_LED": "tp_T_LED",
        }  # Pt100
    elif cfg.params.uv.temperature_pos == "outside":
        keys["T"] = {
            "T_Cuev1": "tp_T_cv1",
            "T_Cuev2": "tp_T_cv2",
            "T_LED": "tp_T_LED",
        }  # NTC

    for mapping in keys.values():
        for src_name, dst_name in mapping.items():
            dat["uv"] = interp1d(
                dat["tp"],  # src
                dat["uv"],  # dst
                ivar_src_name="v25_time",
                ivar_dst_name="v25_time",
                dvar_src_name=src_name,
                dvar_dst_name=dst_name,
                kind="linear",  # no effect with pl_Series_ip1d_lite
                bounds_error=False,  # no effect with pl_Series_ip1d_lite
                fill_value=None,
            )

    # Now we can calculate the temperature-corrected ozone cross section.
    # For that, we need the air temperature first and pressure in the cuvettes.
    # Use a rolling average over the current and the next two elements:
    # value | avg  # value is the average T between the two cuvettes
    # 1     | 2
    # 2     | 3
    # 3     | nan
    # 4     | nan
    dat["uv"] = dat["uv"].with_columns(
        ((pl.col("tp_T_cv1") + pl.col("tp_T_cv2")) / 2)
        .rolling_mean(window_size=3)
        .shift(-2)
        .alias("tp_T_avg")
    )
    # same for cuvette pressure:
    dat["uv"] = dat["uv"].with_columns(
        pl.col("tp_p_cv").rolling_mean(window_size=3).shift(-2).alias("tp_p_avg")
    )
    # now we can use the fit parameters to calculate sigma(T)
    dat["uv"] = dat["uv"].with_columns(
        (
            SIGMA
            * (
                1
                + (
                    TCORRFITPARMS[0]
                    + TCORRFITPARMS[1] * pl.col("tp_T_avg")
                    + TCORRFITPARMS[2] * pl.col("tp_T_avg") ** 2
                    + TCORRFITPARMS[3] * pl.col("tp_T_avg") ** 3
                    + TCORRFITPARMS[4] * pl.col("tp_T_avg") ** 4
                )
            )
        ).alias("sigma_T")
    )

    return dat, cfg


# -----------------------------------------------------------------------------
def _calc_o3(dat, cfg):
    """The actual O3 mr calculation."""
    # For mean and median intensity...
    # Make helper series by shifting given intensity time series 1 and 2 elements
    # backwards.
    # I    | I_shift1 | I_shift2 | MR_result (t_1 = end of sampling interval)
    # I(t) | I(t+1)   | I(t+2)   | mr(t_1)
    for key in ("I_C1", "I_Med_C1", "I_C2", "I_Med_C2"):
        for shift in (1, 2):
            dat["uv"] = dat["uv"].with_columns(
                pl.col(key).shift(shift * -1).alias(f"{key}_shift{shift}")
            )
            # mask intensities below 1e6 counts
            dat["uv"] = dat["uv"].with_columns(
                pl.when(pl.col(f"{key}_shift{shift}") < 1_000_000)
                .then(None)
                .otherwise(pl.col(f"{key}_shift{shift}"))
                .alias(f"{key}_shift{shift}")
            )

    # we need a row number to select every nth row further down (inverted mixing ratios)
    dat["uv"] = dat["uv"].with_row_index()

    # fmt: off
    for mode, pair in zip(
        ("mean", "median"),
        (("I_C1", "I_C2"), ("I_Med_C1", "I_Med_C2"))
    ):
        I_to_I0 = (
            # I (I0):
            dat["uv"].select(
                pl.col(pair[0])
                / pl.col(pair[1])
                + pl.col(pair[0] + "_shift2")
                / pl.col(pair[1] + "_shift2")
            )
            # I0 (I):
            / (
                dat["uv"].select(
                    pl.col(pair[0] + "_shift1")
                    / pl.col(pair[1] + "_shift1")
                )
                * 2
            )
        ).to_series().log() / 2  # divide by 2 since we combined two ratios on numerator and denominator

        dat["uv"] = dat["uv"].with_columns(
            (
                P0 / pl.col("tp_p_avg")  # account for p relative to p0
                * (pl.col("tp_T_avg") + T0) / T0 # account for T relative to T0
                * (1 / (pl.col("sigma_T") * L * N))  # absorption across cuvettes
                * I_to_I0
                * 1e9  # to ppb
            )
            .alias(f"mr_{mode}")
            .fill_nan(None)
        )
        # fmt: on

        # every second element must be inverted because o3 air and zero air
        #   alternate between the two cuvettes:
        dat["uv"] = dat["uv"].with_columns(
            (
                pl.when((pl.col("index") + 1) % 2 == 0)
                .then(pl.col(f"mr_{mode}") * -1)
                .otherwise(pl.col(f"mr_{mode}"))
            ).alias(f"mr_{mode}")
            # .fill_nan(None)
        )

        # set elements that should not be used to Null
        # --> polars: Null = missing data
        #             NaN = not a number, but not missing
        dat["uv"] = dat["uv"].with_columns(
            pl.when(pl.col("flag").eq(Flags_UV.OK))
            .then(dat["uv"][f"mr_{mode}"])
            .otherwise(None)
            .alias(f"mr_{mode}")
        )

        # result might be inverted
        uvmr_median = dat["uv"].select(pl.col(f"mr_{mode}")).to_series().median()
        if uvmr_median is None:
            raise ValueError(
                "Could not deterime median UV mixing ratio. Please make sure to load a valid data set."
            )

        if (
            cfg.params.uv.invert_mr == "auto" and uvmr_median < 0
        ) or cfg.params.uv.invert_mr == "yes":
            dat["uv"] = dat["uv"].with_columns(pl.col(f"mr_{mode}") * -1)
        # implicit else / "no" option --> do not invert

        # force positive MR ?
        if cfg.params.uv.force_positive:
            dat["uv"] = dat["uv"].with_columns(pl.col(f"mr_{mode}").abs())

        # apply LED intensity correction factor
        dat["uv"] = dat["uv"].with_columns(pl.col(f"mr_{mode}") / cfg.params.uv.f_led)

        # apply zero offset
        dat["uv"] = dat["uv"].with_columns(
            pl.col(f"mr_{mode}") - cfg.params.uv.o3_zero_offset
        )

    return dat, cfg


# -----------------------------------------------------------------------------
def _post_process(dat, cfg, _shift_samples: int = -1, _uv_lag_seconds: float = 0.0):
    """Drop helper columns, apply limits, thresholds, timeshift."""
    verboseprint = print if cfg.verbose else lambda *a, **k: None

    dat["uv"] = dat["uv"].drop(
        [
            "index",
            "I_C1_shift1",
            "I_C1_shift2",
            "I_Med_C1_shift1",
            "I_Med_C1_shift2",
            "I_C2_shift1",
            "I_C2_shift2",
            "I_Med_C2_shift1",
            "I_Med_C2_shift2",
            "tp_T_cv1",
            "tp_T_cv2",
            "tp_p_cv",
        ]
    )

    # which result to use as mixing ratio "mr"
    # also, use polars' null instead of nan
    dat["uv"] = dat["uv"].with_columns(
        pl.col(f"mr_{cfg.params.uv.intensity_agg}").fill_nan(None).alias("mr")
    )

    # if there is missing data, flag should signal NOK:
    dat["uv"] = dat["uv"].with_columns(
        pl.when(pl.col("mr").is_null())
        .then(pl.col("flag") | Flags_UV.AUTO_REMOVE)
        .otherwise(pl.col("flag"))
    )

    # apply o3 range limits
    if cfg.params.uv.o3_range:
        dat["uv"] = dat["uv"].with_columns(
            pl.when(
                (pl.col("mr") < cfg.params.uv.o3_range[0])
                | (pl.col("mr") >= cfg.params.uv.o3_range[1])
            )
            .then(pl.col("flag") | Flags_UV.O3_RANGE)
            .otherwise(pl.col("flag"))
        )

    # calculate noise threshold mask
    m = pl.Series([False] * dat["uv"].height)
    if cfg.params.uv.thresh_dI:
        for key in "dI_C1", "dI_C2":
            m = m.set(
                dat["uv"].select(pl.col(key) >= cfg.params.uv.thresh_dI).to_series(),
                True,
            )
    if cfg.params.uv.thresh_dI1I2:
        m = m.set(
            dat["uv"].select(pl.col("dI1_I2") >= cfg.params.uv.thresh_dI1I2).to_series(),
            True,
        )

    if cfg.params.uv.thresh_extend_mask:
        m = pl.Series(extend_mask(m.to_numpy(), cfg.params.uv.thresh_extend_mask + 1))

    # ...and apply the noise threshold mask
    dat["uv"] = dat["uv"].with_columns(
        pl.when(m).then(pl.col("flag") | Flags_UV.NOISE_THRESH).otherwise(pl.col("flag"))
    )

    # apply : T LED range "led_T_range" [min, max]
    if cfg.params.uv.led_T_range:
        dat["uv"] = dat["uv"].with_columns(
            pl.when(
                (pl.col("tp_T_LED") < cfg.params.uv.led_T_range[0])
                | (pl.col("tp_T_LED") >= cfg.params.uv.led_T_range[1])
            )
            .then(pl.col("flag") | Flags_UV.LED_T_RANGE)
            .otherwise(pl.col("flag"))
        )

    # apply : flow range "flow_range" [min, max]
    if cfg.params.uv.flow_range:
        dat["uv"] = dat["uv"].with_columns(
            pl.when(
                (pl.col("tp_F_photom") < cfg.params.uv.flow_range[0])
                | (pl.col("tp_F_photom") >= cfg.params.uv.flow_range[1])
            )
            .then(pl.col("flag") | Flags_UV.FLOW_RANGE)
            .otherwise(pl.col("flag"))
        )

    # calculate diff mr_median-mr_mean
    dat["uv"] = dat["uv"].with_columns(
        pl.when(pl.col("flag") == Flags_UV.OK)
        .then(pl.col("mr_mean") - pl.col("mr_median"))
        .otherwise(None)
        .alias("mr_mean_median_diff")
    )
    cfg.params.derived["uv_mean_median_bias"] = {
        "mean": dat["uv"].select(pl.col("mr_mean_median_diff")).to_series().mean(),
        "median": dat["uv"].select(pl.col("mr_mean_median_diff")).to_series().median(),
        "std": dat["uv"].select(pl.col("mr_mean_median_diff")).to_series().std(),
    }
    if cfg.params.derived.uv_mean_median_bias["std"] is None:
        cfg.params.derived.uv_mean_median_bias["eom"] = None
    else:
        cfg.params.derived.uv_mean_median_bias["eom"] = float(
            cfg.params.derived.uv_mean_median_bias["std"]
            / math.sqrt(dat["uv"].select(pl.col("mr_mean_median_diff").drop_nulls()).shape[0])
        )

    # cut-sections, where UV data should be removed (flagged VMISS) from output
    if cfg.params.uv.output_cut:
        for section in cfg.params.uv.output_cut:
            m = (
                dat["uv"]
                .select((pl.col("mdns") >= section[0]) & (pl.col("mdns") < section[1]))
                .to_series()
            )
            if m.any():
                dat["uv"] = dat["uv"].with_columns(
                    pl.when(m).then(pl.col("flag") | Flags_UV.MAN_REMOVE).otherwise(pl.col("flag"))
                )

    # determine number of valid measurements (flag = 0)
    cfg.params.derived["uv_nvd"] = dat["uv"].select(pl.col("flag") == Flags_UV.OK).to_series().sum()

    # calculate uncertainty; cfg.params.uv.unc_rel and cfg.params.uv.unc_abs_min
    # use what ever is larger for col mr_unc_abs
    dat["uv"] = dat["uv"].with_columns(
        pl.when(pl.col("mr") * cfg.params.uv.unc_rel < cfg.params.uv.unc_abs_min)
        .then(cfg.params.uv.unc_abs_min)
        .otherwise(pl.col("mr") * cfg.params.uv.unc_rel)
        .alias("mr_unc_abs")
    )
    # only where flag = 0
    dat["uv"] = dat["uv"].with_columns(
        pl.when(pl.col("flag").eq(Flags_UV.OK)).then(pl.col("mr_unc_abs")).otherwise(None)
    )

    # modify time axis so that each timestamp denotes the midpoint of the sampling interval
    # shift backwards by one sample to get the correct timestamp, then subtract
    # half the sampling interval to get the mid of the sampling interval.
    dat["uv"] = dat["uv"].with_columns(
        (
            pl.col("v25_time").shift(_shift_samples)
            - pl.duration(seconds=(cfg.params.uv.sampling_period / 2 + _uv_lag_seconds))
        ).alias("v25_time")
    )
    dat["uv"] = dat["uv"].with_columns(
        (
            pl.col("mdns").shift(_shift_samples)
            - (cfg.params.uv.sampling_period / 2 + _uv_lag_seconds)
        ).alias("mdns")
    )

    # check time resolution
    uv_period = (
        dat["uv"]
        .select(pl.col("mdns").filter(pl.col("flag").eq(Flags_UV.OK)).fill_nan(None).diff())
        .to_series()
        .mean()
    )
    if uv_period is None:
        raise ValueError("Could not deterime UV data frequency. Please load valid data.")
    cfg.params.derived["uv_freq_have"] = 1 / uv_period

    if abs(cfg.params.derived["uv_freq_have"] - cfg.params.uv.freq) / cfg.params.uv.freq > 0.1:
        verboseprint(
            "+++++\n"
            "+ Warning:\n"
            "+   UV data frequency deviates from predefined frequncy by more than 10%\n"
            f"+    -> have: {cfg.params.derived['uv_freq_have']:.4f}, want: {cfg.params.uv.freq}"
        )

    return dat, cfg
