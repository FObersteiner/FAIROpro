# -*- coding: utf-8 -*-
"""FAIROpro data input/output handling"""
import pathlib
import re
import shutil
from datetime import datetime, timedelta, timezone

import polars as pl
from pyfuppes.avgbinmap import calc_shift
from pyfuppes.timecorr import filter_dt_forward

# handle import from same directory vs. import from module
try:
    from cfg import CFG  # type: ignore
    from corr_time import parse_reftime  # type: ignore
    from process_cl import Flags_CL  # type: ignore
    from process_uv import Flags_UV  # type: ignore
    from v25 import logs_cleanup
except ModuleNotFoundError:
    from fairopro.cfg import CFG
    from fairopro.corr_time import parse_reftime
    from fairopro.process_cl import Flags_CL
    from fairopro.process_uv import Flags_UV
    from fairopro.v25 import logs_cleanup

try:
    wd = pathlib.Path(__file__).parent
except NameError:
    wd = pathlib.Path.cwd()
assert wd.is_dir(), "faild to obtain working directory"


# -----------------------------------------------------------------------------
TIME_UNIT = "us"  # explicitly use microsecond precision
MICROSECONDS_PER_SECOND = 1e6

# polars config
USE_PYARROW = True

# ALIAS map output name : df column name
ALIAS_OUT_UV = {
    "O3_ppb": "mr",
    "uncertainty_ppb": "mr_unc_abs",
    "cuv_flow_lpm": "tp_F_photom",
    "cuv_T_degrC": "tp_T_avg",
    "cuv_p_hPa": "tp_p_avg",
}
ALIAS_OUT_CL = {"O3_ppb": "cl_mr", "uncertainty_ppb": "mr_unc_abs"}
DT_FMT = "%Y-%m-%dT%H:%M:%S%.3fZ"
DAT_KEYS = ("uv", "cl", "tp", "hsk", "ac")  # dataframes


# -----------------------------------------------------------------------------
# dt_config maps file extension to
#   [0] datetime key, [1] datetime format
dt_config = {
    "uv": ("Time", "%d.%m.%y %H:%M:%S%.f"),
    "cl": ("DateTime", "%d.%m.%y %H:%M:%S%.f"),
    "tp": ("T_pTime", "%d.%m.%y %H:%M:%S%.f"),
    "hsk": ("DateTime", "%d.%m.%y %H:%M:%S%.f"),
    "ac": ("V25_Time", "%d.%m.%y %H:%M:%S%.f"),  # CARIBIC: "V25Time"
}

# input_config maps file extension to
#   [0] header line offset, [1] csv delimiter
input_config = {
    "t_p": (0, "\t"),
    "dat": (0, "\t"),
    "omc": (0, "\t"),
    "osc": (4, "\t"),
    "mas": (0, "\t"),
    "hal": (0, "\t"),
}

# polars data types for all columns
input_dtypes = {
    "dat": {
        "": pl.Utf8,  # trailing sep creates column with empty name...
        "DateTime": pl.Utf8,
        "State": pl.Utf8,
        "Q_LED": pl.Int32,
        "Q_C1_In": pl.Int32,
        "Q_C2_In": pl.Int32,
        "Q_C1": pl.Int32,
        "Q_C2": pl.Int32,
        "Q_O3Scr": pl.Int32,
        "Q_ValvOmc": pl.Int32,
        "Q_ValvBy": pl.Int32,
        "Q_ValvOsc": pl.Int32,
        "Q_Pump": pl.Int32,
        "Q_Van": pl.Int32,
    },
    "hal": {
        "": pl.Utf8,
        "V25_Time": pl.Utf8,
        "HALO_Time": pl.Utf8,
        "Lat": pl.Float64,
        "Lon": pl.Float64,
        "GPS_Alt": pl.Float64,
        "T_Airspeed": pl.Float64,
        "Mach": pl.Float64,
        "Vert_Speed": pl.Float64,
        "Temp": pl.Float64,
        "Dew_Point": pl.Float64,
        "Total_Temp": pl.Float64,
        "Stat_Press": pl.Float64,
        "Dyn_Press": pl.Float64,
        "CabinPress": pl.Float64,
        "Wind": pl.Float64,
    },
    "mas": {
        "": pl.Utf8,
        "MasterTime": pl.Utf8,
        "V25Time": pl.Utf8,
        "MasterState": pl.Utf8,
        "V25State": pl.Utf8,
    },
    "omc": {
        "": pl.Utf8,
        "Time": pl.Utf8,
        "State": pl.Utf8,
        "O3": pl.Float64,
        "I_C1": pl.Int64,
        "I_Med_C1": pl.Int64,  # older instrument data does not have this
        "dI_C1": pl.Int64,
        "I_C2": pl.Int64,
        "I_Med_C2": pl.Int64,  # older instrument data does not have this
        "dI_C2": pl.Int64,
        "dI1_I2": pl.Int64,
        "No": pl.Int32,
        "I_LED": pl.Float64,
        "Temp": pl.Float64,
        "p": pl.Float64,
        "T_C1": pl.Float64,
        "T_C2": pl.Float64,
        "t_PC_ans": pl.Int32,
        "Cuevette": pl.UInt8,
    },
    "osc": {
        "": pl.Utf8,
        "DateTime": pl.Utf8,
        "Time": pl.Float32,
        "I": pl.Int64,
        "dI": pl.Int64,
        "Temp": pl.Float64,
    },
    "t_p": {
        "": pl.Utf8,
        "T_pTime": pl.Utf8,
        "State": pl.Utf8,
        "p_Inlet": pl.Float64,
        "p_Cuev": pl.Float64,
        "p_preOSC": pl.Float64,
        "dp_Oscar": pl.Float64,
        "f_OMCAL": pl.Float64,
        "f_Bypas": pl.Float64,  # CARIBIC O3 does not have this
        "T_LED": pl.Float64,
        "T_C1_In": pl.Float64,
        "T_C2_In": pl.Float64,
        "T_Cuev1": pl.Float64,
        "T_Cuev2": pl.Float64,
        "T_H_In1": pl.Float64,
        "T_H_In2": pl.Float64,
        "T_O3Scr": pl.Float64,
        "T_Pump1": pl.Float64,
        "T_Pump2": pl.Float64,
        "T_In": pl.Float64,
        "T_Van": pl.Float64,
    },
}


# -----------------------------------------------------------------------------
def load_data(cfg: CFG) -> dict:
    """Load data, either from raw data zipfile or parquet files.

    Wraps
        _call_cleaner : call V25 data cleaner (external software)
        _load_v25_logs : load data from V25 log files
        _prepare : make datetime columns etc.
    """
    verboseprint = print if cfg.verbose else lambda *a, **k: None
    dat = {}

    # prerequisites: look for raw data zip and optional directory with pq files
    # zip file must be named "yyyy-mm-dd*.zip"
    name = datetime(*cfg.params.exp_date).strftime("%Y-%m-%d")  # type: ignore

    results = {
        "dir": list(cfg.params.path_input.glob(name)),  # type: ignore
        "zip": list(cfg.params.path_input.glob(f"{name}*.zip")),  # type: ignore
    }

    assert len(results["dir"]) <= 1, "must not find more than one directory"
    assert len(results["zip"]) <= 1, "must not find more than one zipfile"

    if not results["zip"]:
        raise FileNotFoundError(
            f"'{name}.zip' not found in {cfg.params.path_input} - cannot process without raw data"
        )

    # if force_load_from_raw is active and there is a dir, remove it for a clean restart
    if cfg.force_load_from_raw and results["dir"]:
        verboseprint("+++++\n+ Data to be loaded from raw")
        _ = shutil.rmtree(results["dir"][0])
        results["dir"] = []

    # now we made sure there is a zip file. if there is no dir or was removed due to
    # cfg.force_load_from_raw, proceed with processing the zip file.
    if results["dir"]:
        for k in DAT_KEYS:
            files = list(results["dir"][0].glob(f"{name}_{k}.parquet"))
            if files:
                dat[k] = pl.read_parquet(files[0], use_pyarrow=USE_PYARROW)
        if all(v is None for v in dat.values()):
            verboseprint(f"+++++\n+ Found directory '{name}' but no parquet files...")
            shutil.rmtree(results["dir"][0])
            results["dir"] = []
        else:
            verboseprint("+++++\n+ Data loaded from parquet")
            cfg.params["derived"]["_proc_done"]["loaded_from_pq"] = 1

    if not results["dir"]:
        _ = shutil.unpack_archive(results["zip"][0], cfg.params.path_input / name)  # type: ignore

        # find all subdirectories with V25 logs
        srcdirs = [
            d
            for d in (cfg.params.path_input / name).glob("*")  # type: ignore
            if re.match(r"^[0-9]{2}_[0-9]{2}_[0-9]{2}$", d.name)
        ]
        if not srcdirs:
            raise FileNotFoundError(
                f"no subdirectories found with pattern %y_%m_%d in\n{cfg.params.path_input/name}"  # type: ignore
            )
        # directories must contain data
        for d in srcdirs:
            if not any(d.iterdir()):
                raise FileNotFoundError(f"directory {str(d)} is empty")

        cfg.params["derived"]["_srcdirs"] = srcdirs
        _call_cleaner(cfg)  # loading raw data always calls cleaner

        dat = _load_v25_logs(cfg)
        dat = _prepare(dat, cfg)

        _write_input_merge(dat, cfg, "parquet")
        for d in cfg.params["derived"]["_srcdirs"]:
            shutil.rmtree(d)

        cfg.params["derived"]["_proc_done"]["loaded_from_raw"] = 1
        verboseprint("+++++\n+ Data loaded from zip")

    if cfg.params.t_range_exp:
        dat = _cut_exp_t_range(dat, cfg)

    for k in DAT_KEYS:  # fill potentially missing data input with None
        if k not in dat:
            dat[k] = None

    return dat


# -----------------------------------------------------------------------------
def _cut_exp_t_range(dat: dict, cfg: CFG):
    """If t_range_exp parameter is set, truncate the data."""
    t0 = (
        datetime(*cfg.params.exp_date, tzinfo=timezone.utc)  # type: ignore
        + timedelta(seconds=cfg.params.t_range_exp[0])  # type: ignore
    ).replace(tzinfo=None)

    t1 = (
        datetime(*cfg.params.exp_date, tzinfo=timezone.utc)  # type: ignore
        + timedelta(seconds=cfg.params.t_range_exp[1])  # type: ignore
    ).replace(tzinfo=None)

    for k in dat:
        dat[k] = dat[k].filter(pl.col("v25_time").is_between(t0, t1))

    return dat


# -----------------------------------------------------------------------------
def _call_cleaner(
    cfg: CFG,
):
    """Call V25 logfile cleaner for input directories specified in cfg."""
    verboseprint = print if cfg.verbose else lambda *a, **k: None
    paths = cfg.params["derived"]["_srcdirs"]
    assert isinstance(paths, list), f"path(s) must be provided as list, have {type(paths)}"

    # we might have relative paths, i.e. relative to the cfg file
    # cfg parameter loading function must ensure we have valid paths
    for i, p in enumerate(paths):
        if not pathlib.Path(p).is_absolute():
            paths[i] = (pathlib.Path(cfg.params.derived.params_file) / p).resolve()  # type: ignore
        else:
            paths[i] = pathlib.Path(p)

    verboseprint("+++++\n+ Input cleaner:")

    logs_cleanup(paths, verbose=cfg.verbose)  # type: ignore


# -----------------------------------------------------------------------------
def _load_v25_logs(cfg: CFG) -> dict:
    """Load data from V25 logfiles to dict of polars.DataFrames."""
    verboseprint = print if cfg.verbose else lambda *a, **k: None

    # types maps dataframe names to file extensions
    types = {
        "tp": "t_p",  # temperatures and pressures
        "hsk": "dat",  # housekeeping data
        "uv": "omc",  # uv photometer data
        "cl": "osc",  # chemiluminescence data
        "ac": (
            "mas" if cfg.params.exp_platform.upper() == "CARIBIC" else "hal"  # type: ignore
        ),  # aircraft data (IWG1 or master)
    }

    # CARIBIC hack: T_p does not have "f_Bypas"
    if (
        cfg.params.exp_platform.upper() == "CARIBIC"  # type: ignore
        and "f_Bypas" in input_dtypes["t_p"]
    ):
        input_dtypes["t_p"].pop("f_Bypas")

    dat = {}
    for df_name, ext in types.items():
        files = []
        for path in cfg.params["derived"]["_srcdirs"]:
            files += list(pathlib.Path(path).glob("*." + ext.upper()))
            # might be case-sensitive...
            if not files:
                files += list(pathlib.Path(path).glob("*." + ext))

        files = sorted(files)  # this will sort by date/time (path contains date)
        if not files:
            verboseprint(f"+ no data found for extension '{ext}'")
            continue

        dfs = []
        type_spec = input_dtypes[ext]  # column-name : data-type
        for f in files:
            try:  # the try here is just to be able to print the file name in the exception
                tmp = pl.read_csv(
                    f,
                    separator=input_config[ext][1],
                    skip_rows=input_config[ext][0],
                    infer_schema_length=0,  # everything to string
                )
            except Exception as e:
                print(f"\nERROR in '{ext}' file:\n{f}\n")
                raise e

            for (
                col,
                dtype,
            ) in type_spec.items():  # make sure only defined columns are present
                if col in tmp.columns:
                    tmp = tmp.with_columns(tmp[col].str.strip_chars())  # trim spaces
                    # cast to specified type
                    tmp = tmp.with_columns(tmp[col].cast(dtype))
                else:
                    # insert empty column if spec. column is not present
                    tmp = tmp.with_columns(pl.lit(None).alias(col))

            tmp = tmp.select(list(type_spec.keys()))  # drop all other columns

            dfs.append(tmp)

        df = pl.concat(dfs)
        df = df.drop("")  # V25 might make first column empty - drop that.

        dat[df_name] = df

    for k in DAT_KEYS:  # fill potentially missing data input with None
        if k not in dat:
            dat[k] = None

    if all(v is None for v in dat.values()):
        raise ValueError("no data found")

    return dat


# -----------------------------------------------------------------------------
def _prepare(dat: dict, cfg: CFG) -> dict:
    """Prepare loaded V25 data for processing."""
    verboseprint = print if cfg.verbose else lambda *a, **k: None

    # hack for CARIBIC:
    if cfg.params.exp_platform.upper() == "CARIBIC":  # type: ignore
        # V25 time column is named differently
        dt_config["ac"] = ("V25Time", "%d.%m.%y %H:%M:%S%.f")

    # make a common datetime series for all keys
    for key in dat:
        if dat.get(key) is None:
            continue
        dat[key] = (
            dat[key]
            .with_columns(
                pl.col(dt_config[key][0]).str.strptime(
                    pl.Datetime(time_unit="us"), format=dt_config[key][1]
                )
            )
            .rename({dt_config[key][0]: "v25_time"})  # common name for datetime
        )
        # - microsecond precision is fine; V25 has ms at best
        assert (
            dat[key]["v25_time"].dtype.time_unit == TIME_UNIT
        ), f"{key} data: encountered invalid time unit '{dat[key].dtype.time_unit}', want 'us'"

    # uv data from V25 needs to get a more concise name:
    dat["uv"] = dat["uv"].rename({"O3": "V25_O3"})

    # chemiluminescence data needs special treatment:
    # - add "Time" column values as µs to datetime column and drop "Time"
    dat["cl"] = (
        dat["cl"]
        .with_columns(
            pl.col("v25_time") + pl.duration(microseconds=pl.col("Time") * MICROSECONDS_PER_SECOND)
        )
        .drop("Time")
    )

    # now we also want seconds after midnight:
    ref_date = datetime(*cfg.params.exp_date)  # type: ignore
    for key in dat:
        if dat.get(key) is None:
            continue
        dat[key] = dat[key].with_columns(
            (
                (pl.col("v25_time") - ref_date).dt.total_microseconds() / MICROSECONDS_PER_SECOND
            ).alias("mdns")
        )

    # optionally run timestamp correction
    if cfg.params.process_cl and cfg.params.cl.regrid_samples:  # type: ignore
        # create a copy of the original datetime column
        dat["cl"] = dat["cl"].with_columns((pl.col("v25_time")).alias("v25_time_non-regridded"))
        # run correction, overwrite v25_time column
        step = 1 / cfg.params.cl.freq * MICROSECONDS_PER_SECOND  # type: ignore
        dat["cl"] = dat["cl"].with_columns(
            (
                dat["cl"]["v25_time"]
                + pl.Series(
                    calc_shift(
                        dat["cl"]["v25_time"].cast(float).to_numpy(),
                        step=step,
                        lower_bound=-2 * step,
                        upper_bound=3 * step,
                    )
                )
            ).cast(pl.Datetime(time_unit=TIME_UNIT))
        )

    # aircraft data (reference time) needs special treatment:
    # HALO time might contain invalid string (IWG1 string not parsed correctly)
    if cfg.params.process_t_corr and cfg.params.timing.use_t_ref:  # type: ignore
        dat["ac"] = parse_reftime(cfg, dat["ac"])

    # all datetime columns: filter backwards jumps: all t(i) must be > t(i-1)
    for key in dat:
        if dat[key] is None:
            continue
        n, dat[key] = filter_dt_forward(dat[key], datetime_key="v25_time")
        if n != 0:
            verboseprint(
                "+++++\n"
                "+ Datetime filter:\n"
                "+   datetime not increasing strictly monotonocally;\n"
                f"+   removed {n} rows from dataframe '{key}'"
            )

    if cfg.write_raw_merge_csv:
        _write_input_merge(dat, cfg, "csv")

    return dat


# -----------------------------------------------------------------------------
def _write_input_merge(dat: dict, cfg: CFG, output_type: str) -> pathlib.Path:
    """Save csv files with merged input data."""
    t_exp = datetime(*cfg.params.exp_date).strftime("%Y-%m-%d")  # type: ignore
    for name, df in dat.items():
        if df is None:
            continue
        if output_type == "csv":
            df.write_csv(
                cfg.params.path_input / t_exp / f"{t_exp}_{name}.csv",  # type: ignore
                separator="\t",
            )
        elif output_type == "parquet":
            df.write_parquet(
                cfg.params.path_input / t_exp / f"{t_exp}_{name}.parquet",  # type: ignore
                use_pyarrow=USE_PYARROW,
            )
        else:
            raise ValueError(f"invalid output type '{output_type}'")

    return cfg.params.path_input / t_exp  # type: ignore


# -----------------------------------------------------------------------------
def save_data(dat: dict, cfg: CFG) -> pathlib.Path:
    """Save results, excluding flagged values."""

    # TODO : could output all values here, not only where flag == OK

    # verify dst directory exists, make if not
    dst = cfg.params.path_output / "data_save"  # type: ignore
    if not dst.exists():
        dst.mkdir(parents=True)

    # annotate in cfg derived where data is saved and when # type: ignore
    cfg.params.derived["data_saved_to"] = dst.as_posix()  # type: ignore
    now = datetime.now(tz=timezone.utc)
    cfg.params.derived["data_saved_time"] = now.isoformat(timespec="milliseconds")  # type: ignore

    t_exp = datetime(*cfg.params.exp_date).strftime("%Y-%m-%d")  # type: ignore
    t = now.strftime("%Y%m%dT%H%M%SZ")

    # uv data, filename
    fname_uv = f"{t}_{t_exp}_O3-UV.csv"
    # keys to select from data
    keys_uv = ["mdns"] if cfg.time_format == "mdns" else ["v25_time"]
    keys_uv += [v if (k := ALIAS_OUT_UV.get(v)) is None else k for v in cfg.write_params_uv]
    # keys to rename
    if "v25_time" in keys_uv:
        ALIAS_OUT_UV["datetime_UTC"] = "v25_time"
    # extract data and write
    (
        dat["uv"]
        .filter(pl.col("flag").eq(Flags_UV.OK))
        .select(keys_uv)
        .rename({v: k for k, v in ALIAS_OUT_UV.items() if v in keys_uv})
        .write_csv(
            dst / fname_uv,
            include_header=True,
            separator="\t",
            datetime_format=DT_FMT,
            float_precision=cfg.output_float_precision,
            null_value=str(cfg.params.uv.vmiss),  # type: ignore
        )
    )

    if cfg.params.process_cl and cfg.params.derived.uv_nvd > 3:  # type: ignore
        # cl data, filename
        fname_cl = f"{t}_{t_exp}_O3-CL.csv"
        # keys to select from data
        keys_cl = ["mdns"] if cfg.time_format == "mdns" else ["v25_time"]
        keys_cl += [v if (k := ALIAS_OUT_CL.get(v)) is None else k for v in cfg.write_params_cl]
        # keys to rename
        if "v25_time" in keys_cl:
            ALIAS_OUT_CL["datetime_UTC"] = "v25_time"
        # extract data and write
        (
            dat["cl"]
            .filter(pl.col("flag").eq(Flags_CL.OK))
            .select(keys_cl)
            .rename({v: k for k, v in ALIAS_OUT_CL.items() if v in keys_cl})
            .write_csv(
                dst / fname_cl,
                include_header=True,
                separator="\t",
                datetime_format=DT_FMT,
                float_precision=cfg.output_float_precision,
                null_value=str(cfg.params.cl.vmiss),  # type: ignore
            )
        )

    return dst


# -----------------------------------------------------------------------------
def dump_dfs(dat: dict, cfg: CFG, dump_format: str = "csv") -> pathlib.Path:
    """Save dataframes to csvs."""
    # verify dst directory exists, make if not
    dst = cfg.params.path_output / "polars_dfs_dump"  # type: ignore
    if not dst.exists():
        dst.mkdir(parents=True)

    t = datetime.now(tz=timezone.utc).strftime("%Y%m%dT%H%M%SZ")
    t_exp = datetime(*cfg.params.exp_date).strftime("%Y-%m-%d")  # type: ignore

    for k in dat:
        if dump_format == "csv":
            dat[k].write_csv(
                dst / f"{t}_{t_exp}_{k}.csv",
                include_header=True,
                separator="\t",
                datetime_format=DT_FMT,
            )
        elif dump_format == "parquet":
            dat[k].write_parquet(dst / f"{t}_{t_exp}_{k}.parquet", use_pyarrow=USE_PYARROW)
        else:
            raise ValueError(f"invalid dump_format '{dump_format}'")

    return dst
