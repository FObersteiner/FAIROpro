from importlib import metadata

__version__ = metadata.version("fairopro")

from fairopro import (
    cfg,
    corr_time,
    data_io,
    plotter,
    process_cl,
    process_uv,
    v25,
)

__all__ = (
    "cfg",
    "corr_time",
    "data_io",
    "plotter",
    "process_uv",
    "process_cl",
    "v25",
)
