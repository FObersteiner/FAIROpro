import ast
import re
import subprocess
from datetime import datetime, timezone

import pandas as pd
import toml

import fairopro

src = "cfg_keys.ods"
dst_withcomment = "master_cfg.toml"
dst_nocomment = "master_cfg_nocomment.toml"

df = pd.read_excel(src, skiprows=1)

d = {g: {} for g in df["group"].unique() if g not in ("-", "top-level")}  # group: {key: default}


for idx, row in df.iterrows():
    if "remove" in row["change_notes"]:
        continue
    try:
        val = ast.literal_eval(row["default_value"])  # type: ignore
    except Exception as e:
        print(e)
        print(row["default_value"])
        val = row["default_value"]

    if row["group"] == "top-level":
        d[row["new_key_name"]] = val  # type: ignore
    else:
        d[row["group"]][row["new_key_name"]] = val

header = [
    "# FAIROpro config master",
    f"# generated from spreadsheed {src} on {datetime.now(tz=timezone.utc).isoformat()}",
    f"# fairopro package version {fairopro.__version__}",
    "# --- do not edit ---",
    "# ***",
    "# [group]",
    "# KEY: Parameter",
    "# ***",
]

for dst in (dst_withcomment, dst_nocomment):
    with open(dst, "w", encoding="UTF-8") as fp:
        fp.write("\n".join(header) + "\n")

    with open(dst, "a", encoding="UTF-8") as fp:
        toml.dump(d, fp)


# manually add comments to the no-comments-version ----------------------------
content = []
group = "top-level"
with open(dst_nocomment, "r", encoding="UTF-8") as fp:
    for line in fp:
        # skip comments and empty lines
        if line.startswith("#") or line == "\n":
            content.append(line)
            continue
        parts = line.split(" = ")
        if len(parts) != 2:
            result = re.search(r"\[(.*)\]", line, re.IGNORECASE)
            assert result is not None
            group = result.group(1)
            content.append(line)
            continue
        comment = df.loc[(df["group"] == group) & (df["new_key_name"] == parts[0])][
            "description"
        ].iloc[0]
        content.append(f"{line[:-1]} # {comment}\n")

with open(dst_withcomment, "w", encoding="UTF-8") as fp:
    fp.writelines(content)
# -----------------------------------------------------------------------------


for dst in (dst_withcomment, dst_nocomment):
    # verify and format with taplo if available
    # https://taplo.tamasfe.dev/
    try:
        result = subprocess.run(
            f"taplo fmt --option array_auto_expand=false {dst} -- --test",
            shell=True,
            check=True,
            stdout=subprocess.PIPE,
        )
        print(result.stdout.decode("utf-8"))
    except subprocess.CalledProcessError as e:
        print(e)

    # verify that it loads correctly
    with open(dst, "r", encoding="UTF-8") as fp:
        d_reload = toml.load(fp)

    for k, v in d_reload.items():
        assert d.get(k) is not None, f"could not load key '{k}'"
        assert d[k] == v, f"values do not match: {v} != {d[k]}"
