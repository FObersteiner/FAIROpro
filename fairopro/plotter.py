# -*- coding: utf-8 -*-
"""FAIROpro plots"""
from datetime import datetime
from pathlib import Path
from typing import Any, Optional

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import polars as pl
from matplotlib import colormaps as cmaps
from scipy import stats

# handle import from same directory vs. import from module
try:
    from corr_time import Flags_T
    from process_cl import Flags_CL
    from process_uv import Flags_UV
except ModuleNotFoundError:
    from fairopro.corr_time import Flags_T
    from fairopro.process_cl import Flags_CL
    from fairopro.process_uv import Flags_UV

# -----------------------------------------------------------------------------
FIGSIZE = (16, 10)
PIC_TYPE = "png"
PIC_DPI = 300
CM = cmaps["viridis"]
BLOCK = False


# -----------------------------------------------------------------------------
def _extract_plot_labels(plots_list: list[Any]) -> list[str]:
    """Extract label strings from a list of plot objects"""
    return [str(p.get_label()) for p in plots_list]


def _pl_plot_range(
    v: pl.Series,
    add_percent: float = 5,
    v_min_lim: Optional[float] = None,
    v_max_lim: Optional[float] = None,
    xrange: Optional[list] = None,
    x: Optional[pl.Series] = None,
) -> tuple[float, float]:
    """Determine range / limits for a given polars Series v"""
    if x is not None and xrange is not None:
        assert (
            x.shape[0] == v.shape[0]
        ), f"xrange can only be applied if x and v have the same shape\n  got x: {x.shape[0]}, v: {v.shape[0]}"
        v = v.filter((x >= xrange[0]) & (x <= xrange[1]))

    m = pl.Series([True] * v.shape[0])
    if any(v.dtype == t for t in (pl.Float32, pl.Float64)):
        m = ~v.is_nan()
    v = v.filter(m & ~v.is_null())
    if v.shape[0] < 2:  # not enough data
        return (-1.0, 1.0)

    v_min, v_max = v.min(), v.max()
    offset = (abs(v_min) + abs(v_max)) / 2 * add_percent / 100  # type: ignore
    result = [v_min - offset, v_max + offset]  # type: ignore

    if v_min_lim and result[0] < v_min_lim:
        result[0] = v_min_lim

    if v_max_lim and result[1] > v_max_lim:
        result[1] = v_max_lim

    return (result[0], result[1])


# -----------------------------------------------------------------------------
def _get_defaults(dat: dict, cfg, plotname: str, _XRANGE_OFFSET: int = 900):
    """Helper to derive default ranges etc."""
    defaults = {
        "ref_date": datetime(*cfg.params.exp_date),
        "title": "UNKNOWN",
        "xlbl": "UNKNOWN",
        "t_key": "UNKNOWN",
        "xrange": None,
        "xrange_exp": None,
        "datenum2mdns": None,
        "mdns2datenum": None,
    }

    fldate = (
        f"{cfg.params.exp_date[0]:4d}-{cfg.params.exp_date[1]:02d}-{cfg.params.exp_date[2]:02d}"
    )
    defaults["title"] = (
        f"{cfg.params.instrument}, {fldate}" f", '{cfg.params.exp_name}', {plotname}"
    )

    defaults["xlbl"] = f"[s] since {fldate} 00:00 UTC (instr.time)"

    defaults["t_key"] = "mdns_uncorr" if "mdns_uncorr" in dat["uv"].columns else "mdns"

    # if an experiment time range is specified, that is used
    if cfg.params.t_range_exp:
        defaults["xrange"] = [
            cfg.params.t_range_exp[0] - _XRANGE_OFFSET,
            cfg.params.t_range_exp[1] + _XRANGE_OFFSET,
        ]
    else:
        # the default x axis range is derived from the loaded data

        xrange = _pl_plot_range(dat["uv"][defaults["t_key"]], add_percent=0)
        defaults["xrange"] = [xrange[0] - _XRANGE_OFFSET, xrange[1] + _XRANGE_OFFSET]

    # converter functions for secondary x axis, to convert between seconds after
    #   midnight and matplotlib's datenum (days since 1970-01-01)
    defaults["datenum2mdns"] = lambda x: x / 86400 - mdates.date2num(defaults["ref_date"])
    defaults["mdns2datenum"] = lambda x: x / 86400 + mdates.date2num(defaults["ref_date"])

    return defaults


# -----------------------------------------------------------------------------
def _save_fig(fig, fname, cfg):
    """Helper to save the plots."""
    dst = Path(cfg.params.path_output) / "diag_plots"
    if not dst.exists():
        dst.mkdir(parents=True)
    fig.savefig(
        dst / fname,
        dpi=PIC_DPI,
        facecolor="w",
        edgecolor="w",
        orientation="portrait",
        transparent=False,
        pad_inches=0.1,
    )


# -----------------------------------------------------------------------------
def call_4_plots(dat: dict, cfg, close_existing=True):
    """
    Call plots based on the state of data processing.
    """
    data_loaded = (
        cfg.params["derived"]["_proc_done"]["loaded_from_raw"]
        or cfg.params["derived"]["_proc_done"]["loaded_from_pq"]
    )
    uv_calculated = cfg.params["derived"]["_proc_done"]["uv_calc"]
    cl_calculated = cfg.params["derived"]["_proc_done"]["cl_calc"]
    time_corrected = cfg.params["derived"]["_proc_done"]["timecorr"]

    if any((data_loaded, uv_calculated, cl_calculated, time_corrected)) and close_existing:
        plt.close("all")

    if data_loaded:
        temperatures(dat, cfg)
        pressures(dat, cfg)
        flows_cl_dp(dat, cfg)

    if uv_calculated:
        uv_noise(dat, cfg)
        uv_mean_median(dat, cfg)
        flags_uv(dat, cfg)

    if cl_calculated:
        uv_o3_cl_i(dat, cfg)
        cl_sensi(dat, cfg)
        o3_cl_vs_uv(dat, cfg)
        flags_cl(dat, cfg)

    if time_corrected:
        timecorr(dat, cfg)

    if uv_calculated:
        mr_final(dat, cfg)

    # show plots after everything has been rendered:
    plt.show(block=BLOCK)


# fmt: off
# -----------------------------------------------------------------------------
def temperatures(dat: dict, cfg, _plt_no=1):
    f"""{_plt_no} - Temperatures"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: Temperatures")

    # CARIBIC has no T_In
    ydata = (dat["tp"]["T_Van"]
        .append(dat["tp"]["T_C1_In"])
        .append(dat["tp"]["T_C2_In"])
        .append(dat["tp"]["T_Cuev1"])
        .append(dat["tp"]["T_Cuev2"]))
    if cfg.params.exp_platform.upper() != "CARIBIC":
        ydata = ydata.append(dat["tp"]["T_In"])

    yrange0 = _pl_plot_range( # Housing and cuvettes
        ydata,
        add_percent=3,
    )
    yrange1 = _pl_plot_range(  # UV LED temperature
        dat["tp"]["T_LED"],
        add_percent=3,
    )

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # second y-axis
    ax1 = ax0.twinx()

    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    ax0.set_xlabel(defaults["xlbl"], color="k", fontweight="bold")
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax0.set_ylim(yrange0)
    ax0.set_ylabel("T [°C]", color="r", fontweight="bold")
    ax0.tick_params(axis="y", colors="r")
    ax0.grid(axis="y", which="major", linestyle="--", color="r", alpha=0.6)
    ax0.spines["left"].set_color("r")
    ax0.spines["right"].set_color("b")

    # y-axis 1 (right) configuration
    ax1.set_ylim(yrange1)
    ax1.set_ylabel("T LED [°C]", color="b", fontweight="bold")
    ax1.ticklabel_format(style="sci")
    ax1.grid(axis="y", which="major", linestyle="--", color="b", alpha=0.6)
    ax1.tick_params(axis="y", colors="b")
    ax1.spines["left"].set_color("r")
    ax1.spines["right"].set_color("b")

    # plots
    p01 = ax0.plot(dat["tp"][defaults["t_key"]], dat["tp"]["T_Van"], color="g", label="Housing (fan), IN")
    p03 = ax0.plot(dat["tp"][defaults["t_key"]], dat["tp"]["T_C1_In"], color="r", label="inside C1 Pt100")
    p04 = ax0.plot(dat["tp"][defaults["t_key"]], dat["tp"]["T_C2_In"], color="orange", label="inside C2 Pt100")
    p05 = ax0.plot(dat["tp"][defaults["t_key"]], dat["tp"]["T_Cuev1"], color="magenta", label="outside C1 NTC")
    p06 = ax0.plot(dat["tp"][defaults["t_key"]], dat["tp"]["T_Cuev2"], color="purple", label="outside C2 NTC")
    p11 = ax1.plot(dat["tp"][defaults["t_key"]], dat["tp"]["T_LED"], color="b", label="UV LED")

    # specific non-caribic plot
    plots = p01 + p03 + p04 + p05 + p06 + p11
    if cfg.params.exp_platform.upper() != "CARIBIC":
        p02 = ax0.plot(dat["tp"][defaults["t_key"]], dat["tp"]["T_In"], color="grey", label="Housing, OUT")
        plots += p02

    # legend
    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig01_Temperatures.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig



# -----------------------------------------------------------------------------
def pressures(dat: dict, cfg, _plt_no=2):
    f"""{_plt_no} - Pressures"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: Pressures")

    y0 = dat["tp"]["p_Inlet"].append(dat["tp"]["p_Cuev"])
    if dat["ac"] is not None and cfg.params.exp_platform.upper() != "CARIBIC":
        y0 = y0.append(dat["ac"]["Stat_Press"])
    yrange0 = _pl_plot_range(
        y0,
        add_percent=5, v_min_lim=50, v_max_lim=1050
        # xrange=defaults["xrange"],
        # x=,
    )
    yrange1 = _pl_plot_range(
        dat["tp"]["p_preOSC"],
        add_percent=3, v_min_lim=100, v_max_lim=400
        # xrange=defaults["xrange"],
        # x=,
    )

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # second y-axis
    ax1 = ax0.twinx()

    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    ax0.set_xlabel(defaults["xlbl"], color="k", fontweight="bold")
    ax0.set_ylim(yrange0)
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax0.set_ylabel("p [hPa]", color="b", fontweight="bold")
    ax0.tick_params(axis="y", colors="b")
    ax0.grid(axis="y", which="major", linestyle="--", color="b", alpha=0.6)
    ax0.spines["left"].set_color("b")
    ax0.spines["right"].set_color("r")

    # y-axis 1 (right) configuration
    ax1.set_ylim(yrange1)
    ax1.set_ylabel("p [hPa]", color="r", fontweight="bold")
    ax1.grid(axis="y", which="major", linestyle="--", color="r", alpha=0.6)
    ax1.tick_params(axis="y", colors="r")
    ax1.spines["left"].set_color("b")
    ax1.spines["right"].set_color("r")

    # plots
    p0 = ax0.plot(dat["tp"][defaults["t_key"]], dat["tp"]["p_Inlet"], color="g", label="p inlet")
    p1 = ax0.plot(dat["tp"][defaults["t_key"]], dat["tp"]["p_Cuev"], color="b", label="p cuvettes")
    p2 = ax1.plot(dat["tp"][defaults["t_key"]], dat["tp"]["p_preOSC"], color="r", label="p pre-CL")
    plots = p0 + p1 + p2
    if dat["ac"] is not None and cfg.params.exp_platform.upper() != "CARIBIC":
        p3 = ax0.plot(dat["ac"][defaults["t_key"]], dat["ac"]["Stat_Press"], color="cyan", label="A/C p_static")
        plots += p3

    # legend
    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig02_Pressures.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig



# -----------------------------------------------------------------------------
def flows_cl_dp(dat: dict, cfg, _plt_no=3):
    f"""{_plt_no} - CL detector differetial pressure, flows"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: CL-dp + Flows")

    yrange0 = _pl_plot_range(
        dat["tp"]["dp_Oscar"],
        add_percent=5,
        v_min_lim=0, v_max_lim=35,
        xrange=defaults["xrange"],
        x=dat["tp"][defaults["t_key"]],
    )
    yrange1 = _pl_plot_range(
        dat["tp"]["f_OMCAL"].append(dat["tp"]["f_Bypas"]) if "f_Bypas" in dat["tp"].columns else dat["tp"]["f_OMCAL"],
        add_percent=5,
        v_min_lim=0, v_max_lim=25,
        # xrange=defaults["xrange"],
        # x=,
    )

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # second y-axis
    ax1 = ax0.twinx()

    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    ax0.set_ylim(yrange0)
    ax0.set_ylabel("dp [hPa]", color="r", fontweight="bold")
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax0.set_xlabel(defaults["xlbl"], color="k", fontweight="bold")
    ax0.grid(axis="y", which="major", linestyle="--", color="r", alpha=0.6)
    ax0.tick_params(axis="y", colors="r")
    ax0.spines["left"].set_color("r")
    ax0.spines["right"].set_color("g")

    # y-axis 1 (right) configuration
    ax1.set_ylim(yrange1)
    ax1.set_ylabel("flow [vol.L/min]", color="g", fontweight="bold")
    ax1.grid(axis="y", which="major", linestyle="--", color="g", alpha=0.6)
    ax1.tick_params(axis="y", colors="g")
    ax1.spines["left"].set_color("r")
    ax1.spines["right"].set_color("g")

    # plots
    p0 = ax0.plot(dat["tp"][defaults["t_key"]], dat["tp"]["dp_Oscar"], color="r", label="dp CL")
    p1 = ax1.plot(dat["tp"][defaults["t_key"]], dat["tp"]["f_OMCAL"], color="g", label="UV Ph. flow")
    plots = p0 + p1
    if "f_Bypas" in dat["tp"].columns:
        p3 = ax1.plot(dat["tp"][defaults["t_key"]], dat["tp"]["f_Bypas"], color="b", label="Bypass flow")
        plots += p3

    # legend
    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig03_CLdp_Flows.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig



# -----------------------------------------------------------------------------
def uv_noise(dat: dict, cfg, _plt_no=4):
    f"""{_plt_no} - UV photometer noise, O3 as calculated by V25"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: UV Noise")

    ymin, ymax = None, None
    if cfg.params.uv.o3_range:
        ymin, ymax = cfg.params.uv.o3_range

    yrange0 = _pl_plot_range(
        dat["uv"]["V25_O3"].append(dat["uv"]["mr"]),
        add_percent=5,
        v_min_lim=ymin, v_max_lim=ymax,
        # xrange=defaults["xrange"],
        # x=dat["uv"][defaults["t_key"]],
    )
    yrange1 = _pl_plot_range(
        dat["uv"]["dI1_I2"].append(dat["uv"]["dI_C1"]).append(dat["uv"]["dI_C2"]),
        add_percent=10,
        v_min_lim=0.1, v_max_lim=cfg.params.uv.thresh_dI1I2 * 5.0,
        # xrange=defaults["xrange"],
        # x=dat["uv"][defaults["t_key"]],
    )

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # second y-axis
    ax1 = ax0.twinx()

    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    ax0.set_ylim(yrange0)
    ax0.set_xlabel(defaults["xlbl"], color="k", fontweight="bold")
    ax0.set_ylabel("O$_3$ [ppb]", color="k", fontweight="bold")
    ax0.grid(axis="y", which="major", linestyle="--", color="k", alpha=0.6)

    # y-axis 1 (right) configuration
    ax1.set_ylim((0, yrange1[1]))
    ax1.set_ylabel("Noise [cts]", color="g", fontweight="bold")
    ax1.ticklabel_format(style="sci")
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax1.grid(axis="y", which="major", linestyle="--", color="g", alpha=0.6)
    ax1.spines["right"].set_color("g")
    ax1.xaxis.label.set_color("g")
    ax1.tick_params(axis="y", colors="g")

    # plots
    p0 = ax0.plot(dat["uv"][defaults["t_key"]], dat["uv"]["V25_O3"], color="grey", label="V25_O3")
    p1 = ax0.plot(dat["uv"][defaults["t_key"]], dat["uv"]["mr"], color="k", label="O3")
    p2 = ax1.plot(dat["uv"][defaults["t_key"]], dat["uv"]["dI1_I2"], color="g", label="dI1_I2")
    p3 = ax1.plot(dat["uv"][defaults["t_key"]], dat["uv"]["dI_C1"], color="y", label="dI_C1")
    p4 = ax1.plot(dat["uv"][defaults["t_key"]], dat["uv"]["dI_C2"], color="lime", label="dI_C2")
    # mark values where noise exceedes threshold and mixing ratios that are flagged
    p41 = ax1.plot(
        dat["uv"][defaults["t_key"]],
        np.where(
            dat["uv"]["dI1_I2"] > cfg.params.uv.thresh_dI1I2,
            dat["uv"]["dI1_I2"],
            np.nan,
            ), "r+", ms=20.0, label="Noise > Thresh",
    )
    _ = ax1.plot(
        dat["uv"][defaults["t_key"]],
        np.where(
            dat["uv"]["dI_C1"] > cfg.params.uv.thresh_dI, dat["uv"]["dI_C1"], np.nan),
        "r+", ms=20.0,
    )
    _ = ax1.plot(
        dat["uv"][defaults["t_key"]],
        np.where(
            dat["uv"]["dI_C2"] > cfg.params.uv.thresh_dI, dat["uv"]["dI_C2"], np.nan),
        "r+", ms=20.0,
    )
    # p42 = ax0.plot(
    #         dat["uv"][defaults["t_key"]],
    #         np.where(dat["uv"]["flag"].to_numpy() & 4 != 0, dat["uv"]["mr"], np.nan),
    #         "bx", ms=10.0, label="flagged 'remove'"
    #     )

    # legend
    plots = p0 + p1 + p2 + p3 + p4 + p41# + p42
    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig04_UV_Noise.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig



# -----------------------------------------------------------------------------
def uv_mean_median(dat: dict, cfg, _plt_no=5):
    f"""{_plt_no} - UV photometer intensity mean vs. median"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: UV O3 mean-median bias")

    yrange0 = _pl_plot_range(
        dat["uv"]["mr_mean"].append(dat["uv"]["mr_median"]),
        add_percent=3,
        v_min_lim=0, v_max_lim=5000,
        # xrange=defaults["xrange"],
        # x=,
    )
    yrange1 = _pl_plot_range(
        dat["uv"]["mr_median"]-dat["uv"]["mr_mean"],
        add_percent=3,
        v_min_lim=-50, v_max_lim=50,
        # xrange=defaults["xrange"],
        # x=,
    )

    avg_window_10min = int((10*16)/(1/cfg.params.derived.uv_freq_have))

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # second y-axis
    ax1 = ax0.twinx()

    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    ax0.set_ylim(yrange0)
    ax0.set_ylabel("O$_3$ [ppb]", color="k", fontweight="bold")
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax0.set_xlabel(defaults["xlbl"], color="k", fontweight="bold")
    ax0.grid(axis="y", which="major", linestyle="--", color="k", alpha=0.6)
    ax0.tick_params(axis="y", colors="k")
    ax0.spines["left"].set_color("k")
    ax0.spines["right"].set_color("r")

    # y-axis 1 (right) configuration
    ax1.set_ylim(yrange1)
    ax1.set_ylabel("delta_O$_3$ [ppb]", color="r", fontweight="bold")
    ax1.grid(axis="y", which="major", linestyle="--", color="r", alpha=0.6)
    ax1.tick_params(axis="y", colors="r")
    ax1.spines["left"].set_color("k")
    ax1.spines["right"].set_color("r")

    # plots
    p0 = ax0.plot(dat["uv"][defaults["t_key"]], dat["uv"]["mr_median"], color="b", alpha=0.75, label="MR via median")
    p1 = ax0.plot(dat["uv"][defaults["t_key"]], dat["uv"]["mr_mean"], color="g", alpha=0.75, label="MR via mean")
    p2 = ax1.plot(dat["uv"][defaults["t_key"]], dat["uv"]["mr_median"]-dat["uv"]["mr_mean"],
                  color="r", linewidth=1.0, label=(f"MR median - MR mean\n"
                                                   f"mean: {cfg.params.derived.uv_mean_median_bias['mean']:.3f}, "
                                                   f"std: {cfg.params.derived.uv_mean_median_bias['std']:.4f}"))
    p3 = ax1.plot(dat["uv"][defaults["t_key"]],
                  ((dat["uv"]["mr_median"]-dat["uv"]["mr_mean"])
                   .rolling_mean(window_size=avg_window_10min, min_periods=int(avg_window_10min // 2))
                   .shift(-int(avg_window_10min // 2))),
                  color="k", alpha=0.7, label="MR diff running mean")
    _ = ax1.plot([dat["uv"][defaults["t_key"]].min(), dat["uv"][defaults["t_key"]].max()], [0, 0], linestyle="dashed", color="k", alpha=0.5)
    plots = p0 + p1 + p2 + p3

    # legend
    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig05_UV_mean-median.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig



# -----------------------------------------------------------------------------
def uv_o3_cl_i(dat: dict, cfg, _plt_no=6):
    f"""{_plt_no} - O3 as calculated by V25 vs. chemiluminescence intensity"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: CL intensity")

    ymin, ymax = None, None
    if cfg.params.uv.o3_range:
        ymin, ymax = cfg.params.uv.o3_range
    yrange0 = _pl_plot_range(
        dat["uv"]["V25_O3"].append(dat["uv"]["mr"]),
        add_percent=5,
        v_min_lim=ymin, v_max_lim=ymax,
        # xrange=defaults["xrange"],
        # x=dat["uv"][defaults["t_key"]],
    )
    yrange1 = _pl_plot_range(
        dat["cl"]["I"],
        add_percent=5,
        v_min_lim=0.1,
        xrange=defaults["xrange"],
        x=dat["cl"][defaults["t_key"]],
    )

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # second y-axis
    ax1 = ax0.twinx()

    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    ax0.set_ylim(yrange0)
    ax0.set_ylabel("UV O$_3$ [ppb]", color="k", fontweight="bold")
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax0.set_xlabel(defaults["xlbl"], color="k", fontweight="bold")
    ax0.grid(axis="y", which="major", linestyle="--", color="k", alpha=0.6)
    ax0.tick_params(axis="y", colors="k")
    # ax0.spines["left"].set_color("k")
    ax0.spines["right"].set_color("b")

    # y-axis 1 (right) configuration
    ax1.set_ylim(yrange1)
    ax1.set_ylabel("I [cts]", color="b", fontweight="bold")
    ax1.grid(axis="y", which="major", linestyle="--", color="b", alpha=0.6)
    ax1.tick_params(axis="y", colors="b")
    # ax1.spines["left"].set_color("k")
    ax1.spines["right"].set_color("b")

    # plots
    p0 = ax0.plot(dat["uv"][defaults["t_key"]], dat["uv"]["V25_O3"], color="grey", label="V25_O3")
    p1 = ax0.plot(dat["uv"][defaults["t_key"]], dat["uv"]["mr"], color="k", label="O3")
    p2 = ax1.plot(dat["cl"][defaults["t_key"]], dat["cl"]["I"], color="b", label="CL I")

    # legend
    plots = p0 + p1+ p2
    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig06_CL_Int.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig



# -----------------------------------------------------------------------------
def flags_uv(dat: dict, cfg, _plt_no=7):
    f"""{_plt_no} - UV data flags"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: UV data flags")

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    ax0.set_ylabel("UV O$_3$ [ppb]", color="k", fontweight="bold")
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax0.set_xlabel(defaults["xlbl"], color="k", fontweight="bold")
    ax0.grid(axis="y", which="major", linestyle="--", color="k", alpha=0.6)
    ax0.tick_params(axis="y", colors="k")

    # plots
    p0 = ax0.plot(dat["uv"][defaults["t_key"]], dat["uv"]["mr"], color="k", label="O3")

    marker_size = 10

    # flags
    m = np.bitwise_and(dat["uv"]["flag"], Flags_UV.AUTO_REMOVE) != 0
    p1 = ax0.plot(dat["uv"][defaults["t_key"]].filter(m), dat["uv"]["mr"].filter(m).fill_null(0)+1,
                  "b", linestyle='None', marker="+", ms=marker_size, label="FLAG_AUTORMV")

    m = np.bitwise_and(dat["uv"]["flag"], Flags_UV.MAN_REMOVE) != 0
    p2 = ax0.plot(dat["uv"][defaults["t_key"]].filter(m), dat["uv"]["mr"].filter(m).fill_null(0)+2,
                  "g", linestyle='None', marker="*", ms=marker_size, label="FLAG_MANRMV")

    m = np.bitwise_and(dat["uv"]["flag"], Flags_UV.NOISE_THRESH) != 0
    p3 = ax0.plot(dat["uv"][defaults["t_key"]].filter(m), dat["uv"]["mr"].filter(m).fill_null(0)+3,
                  "r", linestyle='None', marker="^", ms=marker_size, label="FLAG_NOISE_THRESH")

    m = np.bitwise_and(dat["uv"]["flag"], Flags_UV.O3_RANGE) != 0
    p4 = ax0.plot(dat["uv"][defaults["t_key"]].filter(m), dat["uv"]["mr"].filter(m).fill_null(0)+4,
                  "cyan", linestyle='None', marker="v", ms=marker_size, label="FLAG_O3_RANGE")

    m = np.bitwise_and(dat["uv"]["flag"], Flags_UV.FLOW_RANGE) != 0
    p5 = ax0.plot(dat["uv"][defaults["t_key"]].filter(m), dat["uv"]["mr"].filter(m).fill_null(0)+5,
                  "magenta", linestyle='None', marker="x", ms=marker_size, label="FLAG_FLOW_RANGE")

    m = np.bitwise_and(dat["uv"]["flag"], Flags_UV.LED_T_RANGE) != 0
    p6 = ax0.plot(dat["uv"][defaults["t_key"]].filter(m), dat["uv"]["mr"].filter(m).fill_null(0)+6,
                  "gold", linestyle='None', marker="*", ms=marker_size, label="FLAG_LED_T_RANGE")

    # legend
    plots = p0 + p1 + p2 + p3 + p4 + p5 + p6
    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig07_UV_flags.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig



# -----------------------------------------------------------------------------
def flags_cl(dat: dict, cfg, _plt_no=8):
    f"""{_plt_no} - CL data flags"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: CL data flags")

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    ax0.set_ylabel("CL I [cts]", color="k", fontweight="bold")
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax0.set_xlabel(defaults["xlbl"], color="k", fontweight="bold")
    ax0.grid(axis="y", which="major", linestyle="--", color="k", alpha=0.6)
    ax0.tick_params(axis="y", colors="k")

    # plots
    p0 = ax0.plot(dat["cl"][defaults["t_key"]], dat["cl"]["I"], color="grey", label="CL intensity")

    marker_size = 10

    m = np.bitwise_and(dat["cl"]["flag"], Flags_CL.AUTO_REMOVE) != 0
    p1 = ax0.plot(dat["cl"][defaults["t_key"]].filter(m), dat["cl"]["I"].filter(m).fill_null(0)+1,
                  "b", linestyle='None', marker="+", ms=marker_size, label="FLAG_AUTORMV")

    m = np.bitwise_and(dat["cl"]["flag"], Flags_CL.MAN_REMOVE) != 0
    p2 = ax0.plot(dat["cl"][defaults["t_key"]].filter(m), dat["cl"]["I"].filter(m).fill_null(0)+2,
                  "g", linestyle='None', marker="*", ms=marker_size, label="FLAG_MANRMV")

    m = np.bitwise_and(dat["cl"]["flag"], Flags_CL.P_RANGE) != 0
    p3 = ax0.plot(dat["cl"][defaults["t_key"]].filter(m), dat["cl"]["I"].filter(m).fill_null(0)+3,
                  "magenta", linestyle='None', marker="x", ms=marker_size, label="FLAG_P_RANGE")

    m = np.bitwise_and(dat["cl"]["flag"], Flags_CL.I_RANGE) != 0
    p4 = ax0.plot(dat["cl"][defaults["t_key"]].filter(m), dat["cl"]["I"].filter(m).fill_null(0)+4,
                  "cyan", linestyle='None', marker="v", ms=marker_size, label="FLAG_I_RANGE")

    m = np.bitwise_and(dat["cl"]["flag"], Flags_CL.I_JUMP) != 0
    p5 = ax0.plot(dat["cl"][defaults["t_key"]].filter(m), dat["cl"]["I"].filter(m).fill_null(0)+5,
                  "r", linestyle='None', marker="o", ms=marker_size, label="FLAG_I_JUMP")

    m = np.bitwise_and(dat["cl"]["flag"], Flags_CL.I_LOF) != 0
    p6 = ax0.plot(dat["cl"][defaults["t_key"]].filter(m), dat["cl"]["I"].filter(m).fill_null(0)+5,
                  "darkred", linestyle='None', marker="o", ms=marker_size, label="FLAG_I_LOF")

    m = np.bitwise_and(dat["cl"]["flag"], Flags_CL.S_RANGE) != 0
    p7 = ax0.plot(dat["cl"][defaults["t_key"]].filter(m), dat["cl"]["I"].filter(m).fill_null(0)+6,
                  "lime", linestyle='None', marker="d", ms=marker_size, label="FLAG_S_RANGE")

    m = np.bitwise_and(dat["cl"]["flag"], Flags_CL.O3_RANGE) != 0
    p8 = ax0.plot(dat["cl"][defaults["t_key"]].filter(m), dat["cl"]["I"].filter(m).fill_null(0)+7,
                  "k", linestyle='None', marker="2", ms=marker_size, label="FLAG_O3_RANGE")

    # legend
    plots = p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8
    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig08_CL_flags.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig



# -----------------------------------------------------------------------------
def cl_sensi(dat: dict, cfg, _plt_no=9):
    f"""{_plt_no} - CL Sensitivity"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: CL Sensitivity")

    yrange0 = _pl_plot_range(
        dat["cl"]["S_smooth"],
        add_percent=5,
        v_min_lim=cfg.params.cl.range_sensi[0], v_max_lim=cfg.params.cl.range_sensi[1],
        xrange=defaults["xrange"],
        x=dat["cl"][defaults["t_key"]],
    )

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # second y-axis
    ax1 = ax0.twinx()

    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    ax0.set_ylim(yrange0)
    ax0.set_ylabel("S [cts/ppb]", color="g", fontweight="bold")
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax0.set_xlabel(defaults["xlbl"], color="k", fontweight="bold")
    ax0.grid(axis="y", which="major", linestyle="--", color="g", alpha=0.6)
    ax0.tick_params(axis="y", colors="g")
    ax0.spines["left"].set_color("g")
    ax0.spines["right"].set_color("b")

    # y-axis 1 (right) configuration
    # ax1.set_ylim(yrange1)
    ax1.set_ylabel("O$_3$ [ppb]", color="b", fontweight="bold")
    ax1.grid(axis="y", which="major", linestyle="--", color="b", alpha=0.6)
    ax1.tick_params(axis="y", colors="b")
    ax1.spines["left"].set_color("g")
    ax1.spines["right"].set_color("b")

    p1 = ax0.plot(dat["cl"][defaults["t_key"]], dat["cl"]["S_corr"], "r", label="S corr. dp", zorder=10)
    p0 = ax0.plot(dat["cl"][defaults["t_key"]], dat["cl"]["S_smooth"], "g", label="S", zorder=11)

    p4 = ax1.plot(dat["uv"][defaults["t_key"]], dat["uv"]["cl_mr"]-dat["uv"]["mr"], "grey", label="CL-UV MR", zorder=1)
    p2 = ax1.plot(dat["cl"][defaults["t_key"]], dat["cl"]["cl_mr"], "b", linewidth=1.0, label="O3 MR, CL", zorder=2)
    p3 = ax1.plot(dat["uv"][defaults["t_key"]], dat["uv"]["mr"], "k", marker="o", ms=1, label="O3 MR, UV", zorder=3)

    ax0.set_frame_on(False)
    ax0.set_zorder(1) # plot on ax0 should be in front of those on ax1

    # legend
    plots = p0 + p1 + p2 + p3 + p4
    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig09_CL_Sensi.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig



# -----------------------------------------------------------------------------
def o3_cl_vs_uv(dat: dict, cfg, _plt_no=10):
    f"""{_plt_no} - UV CL correl"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: UV vs. CL O3 correl")

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # no nulls, no nans, flag == 0
    m = pl.Series([False]*dat["uv"].height)
    m = m.to_frame().select(
             pl.when(
                 (dat["uv"]["flag"] != 0) |
                  dat["uv"]["cl_mr"].is_nan() |
                  dat["uv"]["cl_mr"].is_null() |
                  dat["uv"]["mr"].is_nan() |
                  dat["uv"]["mr"].is_null()
                 ).then(False).otherwise(True)
         ).to_series()

    z = dat["uv"]["cl_mr"].filter(m)-dat["uv"]["mr"].filter(m)

    slope, intercept, r_value, p_value, std_err = stats.linregress(
        dat["uv"]["mr"].filter(m), dat["uv"]["cl_mr"].filter(m)
    )
    p0 = ax0.scatter(dat["uv"]["mr"].filter(m), dat["uv"]["cl_mr"].filter(m),
                     c=z, cmap=CM, label="UV vs. CL")
    plt.colorbar(p0).set_label("(CL \N{MINUS SIGN} UV) O$_3$", rotation=90)

    _ = ax0.plot(
        dat["uv"]["mr"].filter(m),
        dat["uv"]["mr"].filter(m) * slope + intercept,
        color="k",
        linewidth=2,
        label=f"lin. fit r$^2$ = {r_value**2:6.5f}", # type: ignore
    )
    ax0.set_xlabel("UV O$_3$ [ppb]", color="k")
    ax0.set_ylabel("CL O$_3$ [ppb]", color="k")

    # layout & show !
    fig.tight_layout()
    plt.legend(loc=0)
    plt.grid()

    if cfg.write_output:
        _save_fig(fig, f"Fig10_UV_CL_correl.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig



# -----------------------------------------------------------------------------
def timecorr(dat: dict, cfg, _plt_no=11):
    f"""{_plt_no} - Time correction"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: Time correction")

    df_key = "uv" if dat["ac"] is None else "ac"
    ref_key = "mdns" if dat["ac"] is None else "ref_mdns"
    defaults["title"] +=" (manual)" if dat["ac"] is None else defaults["title"]

    y = dat[df_key]["mdns_uncorr"]-dat[df_key][ref_key]
    yrange = _pl_plot_range(
        y,
        add_percent=0.01,
        xrange=defaults["xrange"],
        x=dat[df_key]["mdns"],
    )

    # limit yrange to t_ref_delta_range since values sometimes are drastically out of range
    yrange = ((cfg.params.timing.t_ref_delta_range[0]
                if yrange[0] < cfg.params.timing.t_ref_delta_range[0]
                else yrange[0]),
              (cfg.params.timing.t_ref_delta_range[1]
                if yrange[1] > cfg.params.timing.t_ref_delta_range[1]
                else yrange[1]),
    )

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")

    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    ax0.set_ylim(yrange)
    ax0.set_ylabel("V25-Ref. [s]", color="k", fontweight="bold")
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax0.set_xlabel("instrument time", color="k", fontweight="bold")
    ax0.grid(axis="y", which="major", linestyle="--", color="k", alpha=0.6)
    ax0.tick_params(axis="y", colors="k")

    # plots
    plots = ax0.plot(dat[df_key][defaults["t_key"]], y, color="k", label="time diff.")
    plots += ax0.plot(dat[df_key][defaults["t_key"]], dat[df_key]["t_corr"], linewidth=2,
                  color="r", label="correction")

    if dat["ac"] is not None:
        marker_size = 10
        m = np.bitwise_and(dat["ac"]["flag"], Flags_T.AUTO_REMOVE) != 0
        plots += ax0.plot(dat["ac"][defaults["t_key"]].filter(m), y.filter(m).fill_null(0)+0.3,
                      "g", linestyle='None', marker="+", ms=marker_size, label="FLAG_AUTORMV")

        m = np.bitwise_and(dat["ac"]["flag"], Flags_T.DIFF_RANGE) != 0
        plots += ax0.plot(dat["ac"][defaults["t_key"]].filter(m), y.filter(m).fill_null(0)+0.4,
                      "r", linestyle='None', marker="*", ms=marker_size, label="FLAG_DIFF_RANGE")

        m = np.bitwise_and(dat["ac"]["flag"], Flags_T.REFERENCE_NOK) != 0
        plots += ax0.plot(dat["ac"][defaults["t_key"]].filter(m), y.filter(m).fill_null(0)+0.5,
                      "b", linestyle='None', marker="x", ms=marker_size, label="FLAG_REF_NOK")

        m = np.bitwise_and(dat["ac"]["flag"], Flags_T.MAN_REMOVE) != 0
        plots += ax0.plot(dat["ac"][defaults["t_key"]].filter(m), y.filter(m).fill_null(0)+0.6,
                      "magenta", linestyle='None', marker="v", ms=marker_size, label="FLAG_MANRMV")

        drift = int(cfg.params["derived"]["t_corr_fitparams"][0]*1e6) # µs/s
        # SD of correction vs. the actual difference to reference time
        sd = (
            dat["ac"]
            .select(
                (pl.col("t_corr") - (pl.col("mdns_uncorr") - pl.col("ref_mdns")))
                .drop_nulls()
                .drop_nans()
                .abs()
                .filter(pl.col("flag") == 0)
                * 1e3
            )
            .std()
            .cast(pl.Int32)["t_corr"][0]
            * 2
        )
        ax0.text(
            0.005,
            0.99,
            f"V25 time drift: {drift} µs/s,\n2-fold SD to linear correction: {sd} ms",
            ha="left",
            va="top",
            transform=ax0.transAxes,
            color="red",
            bbox={"facecolor": "none", "edgecolor": "red"},
        )

    # legend
    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig11_timecorr.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig


# -----------------------------------------------------------------------------
def mr_final(dat: dict, cfg, _plt_no=12):
    f"""{_plt_no} - final O3 mr"""
    # prepare settings
    defaults = _get_defaults(dat, cfg, f"{_plt_no}.: Result")

    # prepare figure
    fig, ax0 = plt.subplots(figsize=FIGSIZE)
    fig.suptitle(defaults["title"], fontsize=14, fontweight="bold")


    # y-axis 0 (left) configuration
    ax0.set_xlim(defaults["xrange"]) if defaults["xrange"] is not None else ""
    # ax0.set_ylim(yrange0)
    ax0.set_ylabel("O$_3$ [ppb]", color="k", fontweight="bold")
    ax0.grid(axis="x", which="major", color="k", alpha=0.7)
    ax0.set_xlabel(defaults["xlbl"], color="k", fontweight="bold")
    ax0.grid(axis="y", which="major", linestyle="-", color="k", alpha=0.7)
    ax0.tick_params(axis="y", colors="k")
    ax0.spines["left"].set_color("k")
    ax0.spines["right"].set_color("k")


    m_uv = dat["uv"]["flag"] != 0
    plots = ax0.plot(dat["uv"][defaults["t_key"]].set(m_uv, np.nan), dat["uv"]["mr"].set(m_uv, np.nan),
                     "k", label="UV mr", marker="*", ms=6, zorder=11)
    plots[0].set_markerfacecolor((1, 1, 0, 0.25)) # rgba; barely visible
    if "cl_mr" in dat["cl"].columns:
        m = dat["cl"]["flag"] != 0
        plots += ax0.plot(dat["cl"][defaults["t_key"]].set(m, np.nan), dat["cl"]["cl_mr"].set(m, np.nan),
                          "b", linewidth=1.0, label="CL mr", zorder=10)
        # plots += ax0.plot(dat["uv"][t_key].set(m_uv, np.nan), dat["uv"]["cl_mr"].set(m_uv, np.nan),
        #                   "r", linewidth=2.0, label="CL mapped to UV", zorder=9)
    if "cl_downsam" in dat:
        m = dat["cl_downsam"]["flag"] != 0
        plots += ax0.plot(dat["cl_downsam"][defaults["t_key"]].set(m, np.nan), dat["cl_downsam"]["cl_mr"].set(m, np.nan),
                          "g", label=f"CL mr, {cfg.params.cl.downsample_freq} Hz", zorder=12) # plot in front of rest

    def _extract_plot_labels(plots_list: list[Any]) -> list[str]:
        """Extract label strings from a list of plot objects"""
        return [str(p.get_label()) for p in plots_list]

    fig.legend(plots, _extract_plot_labels(plots))

    # top x-axis that shows hour and minute
    secax = ax0.secondary_xaxis(
        "top", functions=(defaults["datenum2mdns"], defaults["mdns2datenum"])
    )
    secax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    secax.set_xlabel("[hh:mm] UTC", color="k", fontweight="bold")

    # layout & show !
    fig.tight_layout()

    if cfg.write_output:
        _save_fig(fig, f"Fig12_MR_final.{PIC_TYPE}", cfg)
    if cfg.plots_close:
        plt.close()

    return plt, fig


# fmt: on
